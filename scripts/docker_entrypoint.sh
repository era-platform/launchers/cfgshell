#!/bin/bash
set -e

if [ "$1" = 'era' ]; then
    shift
    exec /usr/bin/era "$@"
fi

exec "$@"