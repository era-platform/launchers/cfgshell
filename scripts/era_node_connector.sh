#!/bin/bash

echo "==Era Коннектор нодам=="

platformdir=/usr/lib/era
#$( cd "$( dirname $(dirname "$0") )" && pwd)
pipedir=/var/run/era

echo "Введите номер ноды к которой необходимо подключиться"

#IFS=' ' read -ra T1 <<< `cd $pipedir; ls`
read -d'\n' -ra T1 <<< `cd $pipedir; LC_ALL=C ls`

(cd $pipedir; ls) | cat -n

read nodeidx

node=${T1[$nodeidx-1]}

echo -ne "\033]0;$node\007" && while true; do $platformdir/erlang/bin/to_erl $pipedir/$node/_erlang.pipe; sleep .1; done
