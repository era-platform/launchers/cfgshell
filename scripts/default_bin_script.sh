#!/bin/bash

      export LANGUAGE=en_US:en
      export LANG=en_US.UTF-8
      export TERM=xterm

      shfilepath=/usr/lib/era/installer.sh

      if [ -f $shfilepath ]
         then
         setsid -w $shfilepath $* | tee -a /tmp/installer_bin.log
         else
         echo installer script not found.
exit 1
fi