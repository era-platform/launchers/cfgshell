#!/bin/bash

    scriptdir=$( cd "$( dirname "$0" )" && pwd -P)
    erlangpath=$( cd $scriptdir && pwd -P)/erlang/bin
    escriptfilepath=$(cd $scriptdir/cfgshell/cfgshell/priv && pwd -P)/installer.escript

    export LANGUAGE=en_US:en
    export LANG=en_US.UTF-8
    export TERM=xterm
    export PATH=$erlangpath:$PATH

    setsid -w $erlangpath/escript $escriptfilepath $* | tee -a /tmp/installer.log