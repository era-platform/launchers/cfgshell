%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 17.08.2021 20:00
%%% @doc

-module(cfgshell_adapters_zookeeper).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([connect/4,
         close/1,
         %
         create/2,
         create_r/2,
         get_data/2, get_data_subscr/3,
         set_data/3
        ]).

-export([test/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").
-include_lib("erlzk/include/erlzk.hrl").

-define(ErlZK, erlzk).

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------
%% Tests
%% -----------
test() ->
    MonitorProcess = self(),
    Options = [{chroot, "/"},
               {monitor, MonitorProcess}],
    connect({local,'zoo_local_test'},[{"localhost", 2181}],30000,Options).

%% -----------
%% Connect & Close
%% -----------

%% ---
-spec connect({local, Name::atom()} | {global, GlobalName::term()} | {via, Module::atom(), ViaName::term()},
              [{Host::string(),Port::pos_integer()}], Timeout::pos_integer(), Options::[tuple()]) -> {ok, pid()} | {error, atom()}.
%% ---
connect(ServerName, ServerList, Timeout, Options) ->
    ?ErlZK:connect(ServerName, ServerList, Timeout, Options).

%% ---
-spec close(ConnPid::pid()) -> ok.
%% ---
close(ConnPid) ->
    ?ErlZK:close(ConnPid).

%% -----------
%% Data requests
%% -----------

%% ---
-spec create(Pid::pid(),Path::iodata()) -> {ok, Path::nonempty_string()} | {error, atom()}.
%% ---
create(Pid,Path) ->
    ?ErlZK:create(Pid,Path).

%% ---
-spec create_r(Pid::pid(),Path::binary()) -> {ok, Path::nonempty_string()} | {error, Reason::atom(), Path::nonempty_string()}.
%% ---
%% recursive create node by path
create_r(Pid,Path) when is_binary(Path) ->
    Paths = binary:split(Path,<<"/">>,[global,trim_all]),
    FAcc = fun(PathItem,[]) ->
                   [<<"/",PathItem/binary>>];
              (PathItem,Acc) ->
                   Last = lists:last(Acc),
                   Acc++[<<Last/binary,"/",PathItem/binary>>]
           end,
    DoPaths = lists:foldl(FAcc,[],Paths),
    LastPath = lists:last(DoPaths),
    do_create_r(Pid,DoPaths,LastPath).

%% @private
do_create_r(_,[],LastPath) -> {ok,LastPath};
do_create_r(Pid,[Path|T],LastPath) ->
    case create(Pid,Path) of
        {ok,_Path} -> do_create_r(Pid,T,LastPath);
        {error,node_exists} -> do_create_r(Pid,T,LastPath);
        {error,Reason} -> {error,Reason,Path}
    end.

%% ---
-spec get_data(Pid::pid(),Path::iodata()) -> {ok, Data::binary()} | {error, atom()}.
%% ---
get_data(Pid,Path) ->
    case ?ErlZK:get_data(Pid,Path) of
        {ok,{Data,_}} -> {ok,Data};
        {error,_}=Err -> Err
    end.

%% ---
-spec get_data_subscr(Pid::pid(),Path::iodata(),Watcher::pid()) -> {ok, Data::binary()} | {error, atom()}.
%% ---
get_data_subscr(Pid,Path,Watcher) ->
    case ?ErlZK:get_data(Pid,Path,Watcher) of
        {ok,{Data,_}} -> {ok,Data};
        {error,_}=Err -> Err
    end.

%% ---
-spec set_data(Pid::pid(), Path::iodata(), Data::binary()) -> ok | {error, atom()}.
%% ---
set_data(Pid, Path, Data) ->
    case ?ErlZK:set_data(Pid,Path,Data) of
        {ok,_} -> ok;
        {error,_}=Err -> Err
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================