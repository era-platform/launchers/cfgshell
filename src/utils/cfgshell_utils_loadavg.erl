%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 29.07.2021 5:02
%%% @doc

-module(cfgshell_utils_loadavg).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([get_value_based_on_loadavg/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec get_value_based_on_loadavg(ValueType :: ping_node_timeouts) -> {CallNodeTO::integer(),GenSrvCallTO::integer()};
                                (ValueType :: is_node_load_timeouts) -> {CallNodeTO::integer(),GenSrvCallTO::integer(),SleepBeforeTry::integer()};
                                (ValueType :: ping_node_after_timeout) -> SendAfterTimeout::integer().
%% ---
get_value_based_on_loadavg(ValueType) ->
    FLazy = fun() ->
                    LAvg = os:cmd("cat /proc/loadavg"),
                    {ok, [LAvg1], _} = io_lib:fread("~f ", LAvg),
                    RawCpuInfo = ?BU:to_binary(os:cmd("cat /proc/cpuinfo | grep -e '^physical id' -e '^cpu core'")),
                    RawCpuInfoL = binary:split(RawCpuInfo,<<"\n">>,[global,trim_all]),
                    PhCpuCntL = [?BU:to_int(CpuCores) || {_,CpuCores} <- do_u_zip_cpuinfo(RawCpuInfoL)],
                    PhCpuCnt = lists:sum(PhCpuCntL),
                    LAvg1 / PhCpuCnt
            end,
    Key = {srv_loadavg1_for_one_cpu},
    OptsMap = #{store_timeout => 10000,
                expire_timeout => 5000,
                update_timeout => 1000,
                store_tran => true,
                sync_update => true},
    CpuLAvg1 = ?BLStore:lazy_t(Key, FLazy, OptsMap),
    get_value(ValueType,CpuLAvg1).

%% @private
do_u_zip_cpuinfo(RawCpuInfoL) ->
    do_u_zip_cpuinfo(RawCpuInfoL,[]).

do_u_zip_cpuinfo([],Acc) -> Acc;
do_u_zip_cpuinfo([RawPhysId,RawCpuCores|T],Acc) ->
    FTrim = fun(V) -> string:trim(V) end,
    [_,PhysId] = binary:split(RawPhysId,<<":">>,[trim_all]),
    [_,CpuCores] = binary:split(RawCpuCores,<<":">>,[trim_all]),
    case lists:keymember(PhysId,1,Acc) of
        true -> do_u_zip_cpuinfo(T,Acc);
        false -> do_u_zip_cpuinfo(T,[{PhysId,FTrim(CpuCores)}|Acc])
    end.

%% ---
%% Values for different activities
%% ---
%% All values in milliseconds

%% tout for call node (1st) and call gen_server inside node (2nd value), the values must be less than ping_node_after_timeout values
get_value(ping_node_timeouts,CpuLAvg) when CpuLAvg < 1 -> {900,800};
get_value(ping_node_timeouts,CpuLAvg) when CpuLAvg < 1.5 -> {1800,1600};
get_value(ping_node_timeouts,CpuLAvg) when CpuLAvg < 3 -> {4500,4000};
get_value(ping_node_timeouts,_) -> {13000,11000};
%% tout for call node (1st) and call gen_server inside node (2nd value) and sleep before repeat check (3rd value)
get_value(is_node_load_timeouts,CpuLAvg) when CpuLAvg < 1 -> {900,800,1000};
get_value(is_node_load_timeouts,CpuLAvg) when CpuLAvg < 1.5 -> {1800,1600,2000};
get_value(is_node_load_timeouts,CpuLAvg) when CpuLAvg < 3 -> {4500,4000,4000};
get_value(is_node_load_timeouts,_) -> {13000,11000,12000};
%% send after timeout
get_value(ping_node_after_timeout,CpuLAvg) when CpuLAvg < 1 -> 1000;
get_value(ping_node_after_timeout,CpuLAvg) when CpuLAvg < 1.5 -> 2000;
get_value(ping_node_after_timeout,CpuLAvg) when CpuLAvg < 3 -> 5000;
get_value(ping_node_after_timeout,_) -> 14000.

%% ====================================================================
%% Internal functions
%% ====================================================================