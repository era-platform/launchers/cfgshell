%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 07.08.2021 16:49
%%% @doc

-module(cfgshell_utils_localcfg).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([read_localcfg/0,
         save_localcfg_to_env/1,
         get_localcfg_opts/2,
         %
         srvpath/1, srvpath/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec read_localcfg() ->  {ok,Cfg::map()} | {error,ErrMap::map() | binary()}.
%% ---
read_localcfg() ->
    case ?U:get_localcfg_path() of
        {ok,LocalCfgPath} ->
            case ?U:read_json_file(LocalCfgPath) of
                {ok,LocalCfg}=Ok when is_map(LocalCfg) -> Ok;
                {ok,_} -> {error,<<"invalid localcfg. expected object">>};
                {error,_}=Err -> Err
            end;
        {error,_}=Err -> Err
    end.

%% ---
-spec save_localcfg_to_env(LocalCfg::map()) -> ok.
%% ---
save_localcfg_to_env(LocalCfg) ->
    FSet = fun(K,V) -> ?BU:set_env('local_cfg',?BU:to_atom_new(K),V) end,
    maps:foreach(FSet,LocalCfg).

%% ---
-spec get_localcfg_opts(Key::atom(),Default::term()) -> Value::term() | Default::term().
%% ---
get_localcfg_opts(Key,Default) ->
    ?BU:get_env('local_cfg',Key,Default).

%% ---
-spec srvpath(Key::atom()) -> AbsPath::binary() | no_return().
%% ---
srvpath(Key) ->
    ?BU:to_binary(get_srv_path(Key)).

%% ---
-spec srvpath(Key::atom(),Param::term()) -> AbsPath::binary() | no_return().
%% ---
srvpath(Key,Param) -> ?BU:to_binary(get_srv_path(Key,Param)).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
%% /1
%% basic
get_srv_path(cfgsd) -> ?BU:get_env(local_cfg,config_dir,undefined);
get_srv_path(pipedir) -> ?BU:get_env(local_cfg,pipe_dir,undefined);
get_srv_path(srvwd) -> ?BU:get_env(local_cfg,work_dir,undefined);
get_srv_path(erlroot) -> ?BU:get_env(local_cfg,erl_rootdir,undefined);
get_srv_path(varlog) -> ?BU:get_env(local_cfg,log_dir,undefined);
%% folders & files
get_srv_path(nodepipedir) -> filename:join([get_srv_path(pipedir),?BU:to_binary(node())]);
get_srv_path(nodewd) -> filename:join([get_srv_path(srvwd),?BU:to_binary(node())]);
get_srv_path(lcfg) -> filename:join([get_srv_path(cfgsd),?LocalCfgFileNameWExt]);
%% erlang executables
get_srv_path(erlbin) -> filename:join([get_srv_path(erlroot),<<"bin">>]);
get_srv_path(erl) -> filename:join([get_srv_path(erlbin),<<"erl">>]);
get_srv_path(runerl) -> filename:join([get_srv_path(erlbin),"run_erl"]).

%% /2
%% workdir folder
get_srv_path(nodewd,Node) -> filename:join([get_srv_path(srvwd),?BU:to_binary(Node)]);
get_srv_path(nodelogsyml,Node) -> filename:join([get_srv_path(nodewd,Node),<<"log">>]);
get_srv_path(nodelogd,Node) -> filename:join([get_srv_path(varlog),?BU:to_binary(Node)]);
%% erlang node pipes
get_srv_path(nodepipedir,Node) -> filename:join([get_srv_path(pipedir),?BU:to_binary(Node)]);
%% msvc application dir
get_srv_path(msvcebindir, App) -> filename:join([?U:installdir(),?BU:to_binary(App),?BU:to_binary(App),<<"ebin">>]).