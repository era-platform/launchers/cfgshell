%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 02.01.2021
%%% @doc

-module(cfgshell_utils).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([drop_last_subdir/2,
         ensure_dir/1,
         nodename/0, nodename/1,
         nodehost/0, nodehost/1,
         get_intersect/2,
         ensure_symlink/2,
         %
         read_json_file/1,
         create_json_file/3,
         %
         get_localcfg_path/0,
         installdir/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").
-include_lib("kernel/include/file.hrl").

-define(MLogPrefix, "Cfgshell utils").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec get_localcfg_path() -> {ok,LocalCfgPath::binary()} | {error,Reason::binary()}.
%% ---
get_localcfg_path() ->
    case init:get_argument(local_cfg) of
        {ok,[[LocalCfgPath]]} -> {ok,?BU:to_binary(LocalCfgPath)};
        _ ->
            ?LOG('$trace',"Supv. Init argument 'servernode' is not defined! Stop node."),
            {error,<<"local_cfg parameter missed or invalid">>}
    end.

%% ---
-spec installdir() -> InstallDir::binary() | no_return.
%% ---
installdir() ->
    MayBeSymLinkFolder = ?U:drop_last_subdir(code:which(?MODULE),4),
    ?BU:to_binary(get_mb_symlink_path(MayBeSymLinkFolder)).

%% @private
get_mb_symlink_path(MayBeSymLinkFolder) ->
    case file:read_link_info(MayBeSymLinkFolder) of
        {ok,#file_info{type=directory}} -> MayBeSymLinkFolder;
        {ok,#file_info{type=symlink}} ->
            case file:read_link(MayBeSymLinkFolder) of
                {ok,LinkPath} -> LinkPath;
                {error,_}=Err -> exit(Err)
            end;
        {error,_}=Err -> exit(Err)
    end.

%% ------------------
%% Routines
%% ------------------

%% ---
-spec ensure_dir(DirPath::binary()) -> ok | {error,Reason::file:posix()}.
%% ---
ensure_dir(Path) when is_binary(Path) ->
    PathToCreate = <<Path/binary,"/">>,
    filelib:ensure_dir(PathToCreate).

%% ---
-spec drop_last_subdir(Path::string(),N::integer()) -> NPath::string().
%% ---
drop_last_subdir(Path, 0) -> Path;
drop_last_subdir(Path, N)
  when is_integer(N) ->
    drop_last_subdir(
      lists:droplast(
        lists:reverse(
          lists:dropwhile(fun($/) -> false; (_) -> true end, lists:reverse(Path)))), N-1).

%% ---
-spec nodename() -> NodeName::binary().
%% ---
nodename() ->
    nodename(erlang:node()).

%% ---
-spec nodename(Node::atom()) -> NodeName::binary().
%% ---
nodename(Node) ->
    [NodeName,_] = binary:split(?BU:to_binary(Node),<<"@">>),
    NodeName.

%% ---
-spec nodehost() -> NodeHost::binary().
%% ---
nodehost() ->
    nodehost(erlang:node()).

%% ---
-spec nodehost(Node::atom()) -> NodeHost::binary().
%% ---
nodehost(Node) ->
    [_,NodeHost] = binary:split(?BU:to_binary(Node),<<"@">>),
    NodeHost.

%% ---
-spec get_intersect(List1::list(),List2::list()) -> List3::list().
%% ---
get_intersect(L1,L2) ->
    ordsets:intersection(ordsets:from_list(L1),ordsets:from_list(L2)).

%% ---
-spec ensure_symlink(ExistsPath::binary(),NewLinkPath::binary()) -> ok | {error,Reason::term()}.
%% ---
ensure_symlink(ExistsPath,NewLinkPath) ->
    case is_exists_symlink(ExistsPath,NewLinkPath) of
        true -> ok;
        {false, wrong_dest} ->
            ?OUTL("is_exists_symlink. wrong_dest:~120p",[NewLinkPath]),
            force_del_path(NewLinkPath);
        {false, enoent} -> ok
    end,
    Cmd = <<"ln -fsT ",ExistsPath/binary," ",NewLinkPath/binary," 2>&1">>,
    ?OUTL("ensure_symlink. Cmd:~120p",[Cmd]),
    case ?BU:cmd_exec(Cmd) of
        {0,_} -> ok;
        {_,Reason} -> {error,Reason}
    end.

%% @private
is_exists_symlink(SourcePathBin, SymLinkFolder) ->
    SourcePath = ?BU:to_unicode_list(SourcePathBin),
    ReadLRes = file:read_link(SymLinkFolder),
    case ReadLRes of
        {ok,LinkPath} when LinkPath==SourcePath -> true;
        {ok,_OtherLinkPath} -> {false, wrong_dest};
        {error,enoent} -> {false, enoent};
        {error,_Reason} -> {false, wrong_dest}
    end.

%% @private
force_del_path(Path) ->
    Cmd = <<"rm -rf ",Path/binary>>,
    {0,_} = ?BU:cmd_exec(Cmd),
    ok.

%% ------------------
%% Json files operations
%% ------------------

%% ---
-spec read_json_file(FilePath::binary()) -> {ok, Data::jsx:json_term()} | {error,ErrMap::map()}.
%% ---
read_json_file(FilePath) ->
    ?LOG('$trace',"~p. read_json_file. FilePath:~120p",[?MLogPrefix,FilePath]),
    case file:read_file(FilePath) of
        {ok,DataRaw} ->
            case catch jsx:decode(DataRaw,[return_maps]) of
                {'EXIT',_} -> {error,make_read_error(decode_fail, <<"invalid_json">>)};
                CfgMap -> {ok,CfgMap}
            end;
        {error,Err} -> {error,make_read_error(read_file_error,Err)}
    end.

%% @private
make_read_error(ErrText,ErrReason) ->
    #{error => ErrText, reason => ErrReason}.

%% ---
-spec create_json_file(FilePath::binary(), FileBaseName::binary(), Content::map() | binary()) -> ok | {error,Reason::tuple()}.
%% ---
create_json_file(FilePath, FileBaseName, ContentRaw) when is_binary(ContentRaw) ->
    try jsx:decode(ContentRaw,[return_maps]) of
        Content when is_map(Content) -> create_json_file(FilePath, FileBaseName, Content);
        InvalidContent ->
            ?OUTL("~p. create_json_file. decoded raw content is not map! Content: ~120p",[?MLogPrefix,InvalidContent]),
            {error, {decode_raw_data_error, invalid_type}}
    catch
        C:E:Stack ->
            ?OUTL("~p. create_json_file. decode raw data crash. ~120p:~120p~n~120p",[?MLogPrefix,C,E,Stack]),
            {error,{E,Stack}}
    end;
create_json_file(FilePath, FileBaseName, Content) when is_map(Content) ->
    try jsx:encode(Content,[space,{indent,2}]) of
        ContentBin ->
            FullFilePath = <<(filename:join([FilePath,FileBaseName]))/binary,".json">>,
            ?OUTL("~p. create_json_file. FullFilePath:~120p",[?MLogPrefix,FullFilePath]),
            case file:write_file(FullFilePath,ContentBin) of
                ok -> ok;
                {error,Reason} ->
                    ?OUTL("~p. create_json_file. Error: ~120p",[?MLogPrefix,Reason]),
                    {error, {write_file_error, Reason}}
            end
    catch
        C:E:Stack ->
            ?OUTL("~p. create_json_file. encode data crash. ~120p:~120p~n~120p",[?MLogPrefix,C,E,Stack]),
            {error,{E,Stack}}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================