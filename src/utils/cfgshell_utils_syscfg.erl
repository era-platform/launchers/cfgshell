%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 08.11.2021 17:55

-module(cfgshell_utils_syscfg).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([read_syscfg/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(SysCfgFN, <<"syscfg.json">>).
-define(MLogPrefix, "CfgUtils").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec read_syscfg() -> {ok,SysCfg::map()} | {error,ErrMap::map()}.
%% ---
read_syscfg() ->
    ?LOG('$trace',"~p. read_syscfg.",[?MLogPrefix]),
    SysCfgPath = filename:join([?LCfgU:srvpath(cfgsd),?SysCfgFN]),
    case ?U:read_json_file(SysCfgPath) of
        {ok,SysCfg}=Ok when is_map(SysCfg) -> Ok;
        {ok,_} -> {error,<<"invalid syscfg. expected object">>};
        {error,_}=Err -> Err
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================