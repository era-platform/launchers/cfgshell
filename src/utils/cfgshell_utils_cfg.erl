%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk  <llceceron@gmail.com>
%%% @date 24.09.2021 19:29
%%% @doc

-module(cfgshell_utils_cfg).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([load_cfg/0,
         read_cfg/0
        ]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "CfgUtils").

%% ====================================================================
%% Public functions
%% ====================================================================

% ---
-spec load_cfg() -> {ok,ResMap::map()} | {error,ErrMap::map()}.
%% ---
load_cfg() ->
    ?LOG('$trace',"~p. load_cfg.",[?MLogPrefix]),
    case read_cfg() of
        {ok,CfgMap} ->
            case ?SCfgU:read_syscfg() of
                {ok,SysCfg} -> ?CfgL:load_cfg(CfgMap,SysCfg);
                {error,Err} ->
                    ?LOG('$trace',"~p. load_cfg. read syscfg error: ~120p",[?MLogPrefix,Err]),
                    ?CfgL:load_cfg(CfgMap)
            end;
        {error,_}=Err2 -> Err2
    end.

%% ---
-spec read_cfg() -> {ok,Cfg::map()} | {error,ErrMap::map() | binary()}.
%% ---
read_cfg() ->
    ?LOG('$trace',"~p. read_cfg.",[?MLogPrefix]),
    CfgPath = filename:join([?LCfgU:srvpath(cfgsd),?CfgFNWExt]),
    case ?U:read_json_file(CfgPath) of
        {ok,Cfg}=Ok when is_map(Cfg) -> Ok;
        {ok,_} -> {error,<<"invalid cfg. expected object">>};
        {error,_}=Err -> Err
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================