%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 02.01.2021
%%% @doc

-module(cfgshell_supv).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-behaviour(supervisor).

-export([init/1]).
-export([start_link/1]).
-export([start_child/1, restart_child/1, terminate_child/1, delete_child/1, get_childspec/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("cfgshell.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(StartArgs) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, StartArgs).

start_child(ChildSpec) ->
    supervisor:start_child(?MODULE, ChildSpec).

restart_child(Id) ->
    supervisor:restart_child(?MODULE, Id).

terminate_child(Id) ->
    supervisor:terminate_child(?MODULE, Id).

delete_child(Id) ->
    supervisor:delete_child(?MODULE, Id).

get_childspec(Id) ->
    supervisor:get_childspec(?MODULE, Id).

%% ====================================================================
%% Callback functions
%% ====================================================================

init(StartArgs) ->
    %
    CreateEtsRes = ets:new(?MNodesEts, [named_table,{keypos, #node.name},public]),
    ?LOG('$trace',"Supv. create nodes ets result:~120p",[CreateEtsRes]),
    %
    Specs = [get_child_spec(?MSrv),get_child_spec(?MZSrv)],
    CheckSpecRes = supervisor:check_childspecs(Specs),
    ?LOG('$trace',"Supv. check_childspecs result:~120p",[CheckSpecRes]),
    ?LOG('$trace',"Supv. inited with args:~120p", [StartArgs]),
    SupFlags = #{strategy => one_for_one,
                 intensity => 10,
                 period => 2},
    {ok, {SupFlags, Specs}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% Master srv
get_child_spec(?MSrv) ->
    #{id => ?MSrv,
      start => {?MSrv, start_link, [[]]},
      restart => permanent,
      shutdown => 1000,
      type => worker,
      modules => [?MSrv]};
%% ZooMaster srv
get_child_spec(?MZSrv) ->
    #{id => ?MZSrv,
      start => {?MZSrv, start_link, [[]]},
      restart => permanent,
      shutdown => 1000,
      type => worker,
      modules => [?MZSrv]}.