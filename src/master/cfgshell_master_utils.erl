%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 06.01.2021 15:52
%%% @doc @todo: переделать модуль утилс на конкретные модули - скрипты для старта и т.д.

-module(cfgshell_master_utils).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([get_svcnode_fullname/0,
         get_node_start_cmd/2,
         get_svc_ping_node_start_cmd/1,
         %
         clear_worker/2,
         set_worker/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "MUtils").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------
%% Naming
%% ------------------

%% ---
-spec get_svcnode_fullname() -> Node::atom().
%% ---
get_svcnode_fullname() ->
    Node = erlang:node(),
    ?BU:to_atom_new("serviceping_"++?BU:to_list(Node)).

%% ------------------
%% Nodes start cmds
%% ------------------

%% ---
-spec get_node_start_cmd(MsvcTypes::[binary()],Node::atom()) -> Cmd::binary().
%% ---
get_node_start_cmd(MsvcTypes,Node) ->
    %
    SrvNodeBin = ?BU:to_binary(erlang:node()),
    PipePath = ?LCfgU:srvpath(nodepipedir,Node),
    NodeWorkdir = ?LCfgU:srvpath(nodewd,Node),
    CfgCliMsvcAppDir = ?LCfgU:srvpath(msvcebindir, ?APPCfgStarter),
    ErlPath = ?LCfgU:srvpath(erl),
    RunErlPath = ?LCfgU:srvpath(runerl),
    LCfgPath = ?LCfgU:srvpath(lcfg),
    %
    {NodeProcParams,ErlMsvcParams} = get_node_cmd_params(MsvcTypes,{<<>>,<<>>}),
    ?LOG('$trace',"~p. get_node_start_cmd. os_cmd_params: ~120tp~nerl_cmd_params: ~120tp",[?MLogPrefix,NodeProcParams,ErlMsvcParams]),
    Cookie = ?BU:to_binary(erlang:get_cookie()),
    %
    <<"/bin/bash -c '",NodeProcParams/binary," && ",
      RunErlPath/binary," -daemon ",
      PipePath/binary,"/_erlang.pipe ",
      NodeWorkdir/binary," \"cd ",
      NodeWorkdir/binary," && ",ErlPath/binary,
      " -name ",(?BU:to_binary(Node))/binary,
      " -setcookie ",Cookie/binary,
      " -run ",(?BU:to_binary(?APPCfgStarter))/binary,
      " -servernode ",SrvNodeBin/binary,
      " -local_cfg ",LCfgPath/binary,
      " +pc unicode",
      " -hidden",
      " +fnu",
      % "-kernel inet_dist_listen_min ",?BU:to_list(NodePortMin)," inet_dist_listen_max ",?BU:to_list(NodePortMax)," ", % @TODO add ports
      " ",ErlMsvcParams/binary,
      " -shutdown_time 2000",
      " -pa ", CfgCliMsvcAppDir/binary," \" && echo $! 2>&1",
      "'">>. % close bash -c

%% ---
-spec get_svc_ping_node_start_cmd(Node::atom()) -> Cmd::binary().
%% ---
get_svc_ping_node_start_cmd(Node) ->
    %
    SrvNodeBin = ?BU:to_binary(erlang:node()),
    PipePath = ?LCfgU:srvpath(nodepipedir,Node),
    NodeWorkdir = ?LCfgU:srvpath(nodewd,Node),
    ErlPath = ?LCfgU:srvpath(erl),
    RunErlPath = ?LCfgU:srvpath(runerl),
    %
    Cookie = ?BU:to_binary(erlang:get_cookie()),
    %
    <<RunErlPath/binary," -daemon ",
      PipePath/binary,"/_erlang.pipe ",
      NodeWorkdir/binary," \"cd ",
      NodeWorkdir/binary," && ",ErlPath/binary,
      " -name ",(?BU:to_binary(Node))/binary,
      " -setcookie ",Cookie/binary," ",
      " -servernode ",SrvNodeBin/binary,
      " +pc unicode ",
      " -hidden",
      " +fnu ",
      % "-kernel inet_dist_listen_min ",?BU:to_list(NodePortMin)," inet_dist_listen_max ",?BU:to_list(NodePortMax)," ", % @TODO add ports
      " -shutdown_time 2000",
      " \" && echo $! 2>&1">>.

%% ------------------
%% Routines
%% ------------------

%% ---
-spec clear_worker(Type::atom(), State::#msrvstate{}) -> State1::#msrvstate{}.
%% ---
clear_worker(cfg_load,#msrvstate{workers=W}=State) ->
    State#msrvstate{workers=W#msworkers{cfg_load=undefined}};
clear_worker(node_refresh, #msrvstate{workers=W}=State) ->
    State#msrvstate{workers=W#msworkers{node_refresh=undefined}};
clear_worker(check_ping,#msrvstate{workers=W}=State) ->
    State#msrvstate{workers=W#msworkers{check_ping=undefined}};
clear_worker(analyze_svcnode_ping, #msrvstate{workers=W}=State) ->
    State#msrvstate{workers=W#msworkers{analyze_svcnode_ping=undefined}}.

%% ---
-spec set_worker(Type::atom(), Worker::{pid(),reference()}, State::#msrvstate{}) -> State1::#msrvstate{}.
%% ---
set_worker(cfg_load, Worker, #msrvstate{workers=W}=State) ->
    State#msrvstate{workers=W#msworkers{cfg_load=Worker}};
set_worker(node_refresh, Worker, #msrvstate{workers=W}=State) ->
    State#msrvstate{workers=W#msworkers{node_refresh=Worker}};
set_worker(check_ping, Worker, #msrvstate{workers=W}=State) ->
    State#msrvstate{workers=W#msworkers{check_ping=Worker}};
set_worker(analyze_svcnode_ping, Worker, #msrvstate{workers=W}=State) ->
    State#msrvstate{workers=W#msworkers{analyze_svcnode_ping=Worker}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
get_node_cmd_params([],{NodeProcParams,ErlMsvcParams}) -> {NodeProcParams,ErlMsvcParams};
get_node_cmd_params([MsvcType|T],{NodeProcParamsAcc,ErlMsvcParamsAcc}) ->
    [NodeProcParams,ErlMsvcParams] = ?CfgL:get_msvc_descriptor_info(MsvcType,[os_cmd_params,erl_cmd_params]),
    NodeProcParamsAcc1 = <<NodeProcParamsAcc/binary," ",NodeProcParams/binary>>,
    ErlMsvcParamsAcc1 = <<ErlMsvcParamsAcc/binary," ",ErlMsvcParams/binary>>,
    get_node_cmd_params(T,{NodeProcParamsAcc1,ErlMsvcParamsAcc1}).
