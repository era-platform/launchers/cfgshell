%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 02.01.2021
%%% @doc

-module(cfgshell_master_srv).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-behaviour(gen_server).

-export([start_link/1,
         %
         init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3,
         %
         call/2,
         cast/1, ext_cast/1,
         send_after/2
        ]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("cfgshell.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, Opts, []).

%% ---
-spec call(Req::term(),Timeout::integer()) -> {ok,Result::map()} | {error,Reason::binary()}.
%% ---
call(Req,Timeout) ->
    gen_server:call(?MSrv,Req,Timeout).

%% ---
-spec cast(Request::term()) -> ok.
%% ---
cast(Request) ->
    gen_server:cast(?MODULE,Request).

%% ---
-spec ext_cast(Request::term()) -> ok.
%% ---
ext_cast(Request) ->
    gen_server:cast(?MODULE,{?ME,Request}).

%% ---
-spec send_after(Req::term(),Timeout::integer()) -> Ref::reference().
%% ---
send_after(Req,Timeout) ->
    erlang:send_after(Timeout,?MSrv,Req).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Opts) ->
    Ref = erlang:make_ref(),
    State = #msrvstate{state=?MStI,
                       nodes_ets=?MNodesEts,
                       ref=Ref},
    erlang:send_after(0,self(),{?ME,load_cfg,Ref}),
    ?LOG('$trace',"cfgshell_master_srv. inited"),
    ?LOG('$trace',"~p. inited",[?MODULE]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% installer requests. load_cfg
handle_call({installer_request,reload_cfg,_MapOpts}, From, State) ->
    ?LOG('$trace',"~p. rcv installer_request reload_cfg",[?MODULE]),
    MapOpts = #{from => From},
    State1 = ?MFsm:handle_event({?ME, reload_cfg}, MapOpts, State),
    {noreply, State1};

handle_call(_Request, _From, State) ->
    ?LOG('$trace',"~p. rcv unknown CALL:~120p",[?MODULE,_Request]),
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({?NE, _Event, NodeName}=Req, State) ->
    ?BWSrv:cast_work(NodeName, {?MFsm,handle_node_event,[Req,State]}),
    {noreply, State};

handle_cast({?ME, Event, Ref}, #msrvstate{ref=Ref}=State) ->
    State1 = ?MFsm:handle_event({?ME, Event}, State),
    {noreply, State1};

%%
handle_cast({?ME, Event}, State) ->
    State1 = ?MFsm:handle_event({?ME, Event}, State),
    {noreply, State1};

%% Other
handle_cast(_Request, State) ->
    ?LOG('$trace',"~p. rcv unknown CAST:~120p",[?MODULE,_Request]),
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% node fsm events
handle_info({?NE, _Event, NodeName}=Req, State) ->
    ?BWSrv:cast_work(NodeName, {?MFsm, handle_node_event, [Req,State]}),
    {noreply, State};

%% master fsm events
handle_info({?ME, Event, Ref}, #msrvstate{ref=Ref}=State) ->
    State1 = ?MFsm:handle_event({?ME, Event}, State),
    {noreply, State1};

handle_info({'DOWN',_Ref,process,_Pid,_Error}=Req, State) ->
    State1 = ?MFsm:handle_event({?ME,{proc_down,Req}}, State),
    {noreply,State1};

%% default
handle_info(_Info, State) ->
    ?LOG('$trace',"~p. rcv unknown INFO:~120p",[?MODULE,_Info]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================
