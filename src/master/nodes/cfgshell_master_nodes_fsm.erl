%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 22.02.2021 11:38
%%% @doc

-module(cfgshell_master_nodes_fsm).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([handle_event/2,
         handle_event/3,
         %
         initing/3,
         working/3,
         stopped/3,
         %
         init_node/1,
         %
         multiparse_noderec/2,
         parse_noderec/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "Node Fsm").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec handle_event(Event::term(), NodeRec::#node{}) ->
          ok | {ok,NodeRec1::#node{}} | {error,NodeRec::#node{},Reason::binary()}.
%% ---
handle_event(Event, #node{state=St}=NodeRec) ->
    handle_event(Event, #node{state=St}=NodeRec, undefined).

%% ---
-spec handle_event(Event::term(), NodeRec::#node{}, State::#msrvstate{} | undefined) ->
          ok | {ok,NodeRec1::#node{}} | {error,NodeRec::#node{},Reason::binary()}.
%% ---
handle_event(Event, #node{name=Name,state=St}=NodeRec, State) ->
    ?LOG('$trace',"~p. <-- Event (~120p) node_state (~120p) before: ~120p",[?MLogPrefix,Event,Name,St]),
    Result = ?MODULE:St(Event,NodeRec,State),
    St1 = get_out_node_state(Result,NodeRec),
    ?LOG('$trace',"~p. --> Event (~120p) node_state (~120p) after: ~120p",[?MLogPrefix,Event,Name,St1]),
    Result.

%% @private
get_out_node_state(ok,#node{state=St}=_OldNodeRec) -> St;
get_out_node_state({ok,#node{state=St}},_OldNodeRec) -> St;
get_out_node_state({error,#node{state=St},_Reason},_OldNodeRec) -> St.

%% ---
-spec init_node(NodeData::map()) -> NodeRec:: #node{} | no_return().
%% ---
init_node(NodeData) when is_map(NodeData) ->
    create_node_rec(NodeData).

%% ---
-spec multiparse_noderec(RecordKeys::[atom()], NodeRec::#node{}) -> Value::term() | no_return().
%% ---
multiparse_noderec(RecordKeys,NodeRec) when is_list(RecordKeys) ->
    F = fun(RecKey) -> parse_noderec(RecKey,NodeRec) end,
    lists:map(F,RecordKeys).

%% ---
-spec parse_noderec(RecordKey::atom(), NodeRec::#node{}) -> Value::term() | no_return().
%% ---
parse_noderec(nodename,#node{name=NodeName}) -> NodeName;
parse_noderec(node,#node{node=Node}) -> Node;
parse_noderec(msvcdata,#node{msvc_data=MsvcData}) -> MsvcData;
parse_noderec(ping_state,#node{ping_state=#ping_state{state=ok}}) -> ok;
parse_noderec(ping_state,#node{ping_state=#ping_state{state=error,error_cnt=ErrCnt}}) -> {error,ErrCnt}.

%% ====================================================================
%% FSM
%% ====================================================================

%% ---
%% initing
%% ---

%% останавливаем предыдущие реинкорнации ноды
initing(check_old_node_is_stopped,NodeRec,_State) ->
    stop_old_node(NodeRec);
%% если запуск ноды не удачен, стейт не изменяется, перезапуск выполняет fsm сервера
initing(start_node,NodeRec,_State) ->
    start_node(NodeRec);
initing(get_cfg,NodeRec,_State) ->
    send_cfg(NodeRec);
%% в случае если нода не запустилась с сохранением состояния
initing({ping_result,PingResult},NodeRec,_State) ->
    handle_ping_result(NodeRec,PingResult);
%% если пингуется служебная нода, а текущая - нет, то нода будет перезапущена
initing(restart_by_ping,NodeRec,_State) ->
    restart_node(NodeRec);
%% если не пингуется даже служебная нода, то нода будет остановлена
initing(stop_node,NodeRec,_State) ->
    stop_node(NodeRec).


%% ---
%% working
%% ---

working({update_node,MsvcData},NodeRec,_State) ->
    update_node(NodeRec,MsvcData);
working(get_cfg,NodeRec,_State) ->
    send_cfg(NodeRec);
working({ping_result,PingResult},NodeRec,_State) ->
    handle_ping_result(NodeRec,PingResult);
working(restart_by_ping,NodeRec,_State) ->
    restart_node(NodeRec);
working(stop_node,NodeRec,_State) ->
    stop_node(NodeRec).

%% ---
%% stopped
%% ---

%% восстановление если служебная нода стала пинговаться
stopped(resume_node,NodeRec,_State) ->
    resume_node(NodeRec).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------
%% Create node record
%% ------------------

%% ---
create_node_rec(NodeData) ->
    NodeName = maps:get(<<"name">>,NodeData),
    ?LOG('$trace',"~p. create_node_rec for NodeName: ~120p",[?MLogPrefix,NodeName]),
    #node{name = NodeName,
          state = ?NStI,
          host = maps:get(<<"host">>,NodeData),
          node = maps:get(<<"node">>,NodeData),
          start_cmd = get_node_start_cmd(NodeData),
          msvc_data = NodeData}.

%% @private
get_node_start_cmd(NodeData) ->
    [Msvcs,Node,NodeName] = ?BU:maps_get([<<"microservices">>,<<"node">>,<<"name">>],NodeData),
    MsvcTypes = [maps:get(type,MsvcData) || MsvcData <- Msvcs],
    ?LOG('$trace',"~p. get_node_start_cmd for NodeName: ~120p on node: ~120p",[?MLogPrefix,NodeName,Node]),
    ?MU:get_node_start_cmd(MsvcTypes,Node).

%% ------------------
%% Stop node
%% ------------------

%% ---
stop_old_node(NodeRec) ->
    #node{name=NodeName,state=State}=NodeRec,
    ?LOG('$trace',"~p. stop_node. nodename:~120p; state:~120p",[?MLogPrefix,NodeName,State]),
    NodePids = get_node_pid(NodeName),
    ?LOG('$trace',"~p. stop_node. get_node_pid result:~120p",[?MLogPrefix,NodePids]),
    case NodePids of
        {ok,not_found} -> ok;
        {ok,OsNodePids} -> proc_kill(OsNodePids);
        {error,_} -> ok
    end,
    ok.

%% ---
stop_node(NodeRec) ->
    #node{name=NodeName,state=State}=NodeRec,
    ?LOG('$trace',"~p. stop_node. nodename:~120p; state:~120p",[?MLogPrefix,NodeName,State]),
    % @TODO add call node and call init stop, after that - kill node
    NodePids = get_node_pid(NodeName),
    ?LOG('$trace',"~p. stop_node. get_node_pid result:~120p",[?MLogPrefix,NodePids]),
    %
    StoppedNodeRec = NodeRec#node{state=stopped,ping_state=#ping_state{}},
    case NodePids of
        {ok,not_found} ->
            ?LOG('$info',"~p. stop_node. Stop NodeName (~120p) success",[?MLogPrefix,NodeName]),
            {ok,StoppedNodeRec};
        {ok,OsNodePids} ->
            proc_kill(OsNodePids),
            ?LOG('$info',"~p. stop_node. Stop NodeName (~120p) success",[?MLogPrefix,NodeName]),
            {ok,StoppedNodeRec};
        {error,_} ->
            Reason = <<"stop_node. get node pids error">>,
            ?LOG('$info',"~p. stop_node. Stop NodeName (~120p) error: ~120p",[?MLogPrefix,NodeName,Reason]),
            {error,Reason}
    end.

%% @private
get_node_pid(NodeName) ->
    Cmd = <<"ps -ef | awk '[-]name ",NodeName/binary,"@/ {print $0}' | grep -v awk | awk '{print $2}'">>,
    ?LOG('$trace',"~p. get_node_pid. nodename:~120p~nCmd:~120p",[?MLogPrefix,NodeName,Cmd]),
    case ?BU:cmd_exec(Cmd) of
        {0,<<>>} -> {ok, not_found};
        {0,Res} -> {ok, binary:split(Res,<<"\n">>,[global,trim_all])};
        {ErrCode,ErrMsg} ->
            ?LOG('$trace',"~p. get_node_pid. for '~120p'. ErrCode:~120p, ErrMsg:~120p",[?MLogPrefix,NodeName,ErrCode,ErrMsg]),
            {error, ErrMsg}
    end.

%% @private
proc_kill([]) -> ok;
proc_kill([SystemPid|T]) ->
    CmdKillProc = <<"kill -9 ",SystemPid/binary," 2>&1">>,
    _KillRes = ?BU:cmd_exec(CmdKillProc),
    ?LOG('$trace',"~p. proc_kill. kill proc:~120p result:~120p",[?MLogPrefix,SystemPid,_KillRes]),
    proc_kill(T).

%% ------------------
%% Update node
%% ------------------

%% ---
update_node(OldNodeRec,_MsvcData) ->
    ?LOG('$trace',"~p. update_node.",[?MLogPrefix]),
    {ok, OldNodeRec}. %% @todo implement

%% ------------------
%% Start node
%% ------------------

%% ---
start_node(NodeRec) ->
    #node{name=NodeName}=NodeRec,
    ?LOG('$trace',"~p. start_node.",[?MLogPrefix]),
    do_start_node(NodeRec),
    case is_node_load(NodeRec) of
        true ->
            ?LOG('$info',"~p. start_node. Start NodeName (~120p) success",[?MLogPrefix,NodeName]),
            {ok,NodeRec#node{state=?NStW}};
        false ->
            ok = stop_old_node(NodeRec),
            ?LOG('$info',"~p. start_node. Start NodeName (~120p) error: ~120p",[?MLogPrefix,NodeName,<<"node_stop_after_load_fail">>]),
            {error,NodeRec,<<"node_stop_after_load_fail">>}
    end.

%% @private
do_start_node(NodeRec) ->
    #node{node=Node,start_cmd=Cmd}=NodeRec,
    ?LOG('$trace',"~p. do_start_node. node:~120p",[?MLogPrefix,Node]),
    PipePath = ?LCfgU:srvpath(nodepipedir,Node),
    NodeWorkdir = ?LCfgU:srvpath(nodewd,Node),
    NodeLogDir = ?LCfgU:srvpath(nodelogd,Node),
    NodeSymlLogDir = ?LCfgU:srvpath(nodelogsyml,Node),
    %
    EnsurePipeDirRes = ?U:ensure_dir(PipePath),
    ?LOG('$trace',"~p. do_start_node. ensure pipe dir: ~120p. result:~120p",[?MLogPrefix,PipePath,EnsurePipeDirRes]),
    EnsureNodeWdRes = ?U:ensure_dir(NodeWorkdir),
    ?LOG('$trace',"~p. do_start_node. ensure node log dir: ~120p. result:~120p",[?MLogPrefix,NodeWorkdir,EnsureNodeWdRes]),
    EnsureNodeWdRes = ?U:ensure_dir(NodeLogDir),
    ?LOG('$trace',"~p. do_start_node. ensure node work dir: ~120p. result:~120p",[?MLogPrefix,NodeLogDir,EnsureNodeWdRes]),
    EnsureLogSlRes = ?U:ensure_symlink(NodeLogDir,NodeSymlLogDir),
    ?LOG('$trace',"~p. do_start_node. ensure node log dir symink.~nfrom: ~120p to ~120p. result:~120p",[?MLogPrefix,NodeSymlLogDir,NodeLogDir,EnsureLogSlRes]),
    %
    _CmdRes = ?BU:cmd_exec(Cmd), %% @todo handle error
    ?LOG('$trace',"~p. do_start_node. start node:~120p result:~120p~ncmd:~120p",[?MLogPrefix,Node,_CmdRes,Cmd]),
    ok.

%% @private
is_node_load(NodeRec) ->
    #node{node=Node}=NodeRec,
    ?LOG('$trace',"~p. is_node_load. start for node:~120p",[?MLogPrefix,Node]),
    is_node_load(NodeRec,6).

%% @private
is_node_load(_,CntTry) when CntTry <0 -> false;
is_node_load(NodeRec,CntTry) ->
    #node{node=Node}=NodeRec,
    {CallNodeTO, CallGenSrvTO, SleepTO} = ?LAvgU:get_value_based_on_loadavg(is_node_load_timeouts),
    MapOpts = #{timeout => CallGenSrvTO},
    case ?BRpc:call(Node, ?APPCfgStarter, is_node_load, [MapOpts], CallNodeTO) of
        true -> true;
        {badrpc, Reason} ->
            ?LOG('$trace',"~p. is_node_load. call node:~120p badrpc:~120p",[?MLogPrefix,Node,Reason]),
            timer:sleep(SleepTO),
            is_node_load(NodeRec,CntTry-1)
    end.

%% ------------------
%% Send cfg to node
%% ------------------

%% ---
send_cfg(NodeRec) ->
    #node{name=NodeName,node=Node}=NodeRec,
    ?LOG('$trace',"~p. send_cfg. send cfg to node:~120p",[?MLogPrefix,Node]),
    F = fun() ->
                Cfg = ?CfgL:get_curr_cfg(),
                ?BRpc:cast(Node, ?APPCfgStarter, take_cfg, [Cfg])
        end,
    ?BWSrv:call_workf(NodeName, F),
    ok.

%% ------------------
%% Handle node ping result
%% ------------------

%% ---
handle_ping_result(NodeRec,PingResult) ->
    #node{node=Node,ping_state=PingState}=NodeRec,
    %?LOG('$trace',"~p. handle_ping_result. Node:~120p; PingResult:~120p",[?MLogPrefix,Node,PingResult]),
    #ping_state{state=PState,error_cnt=ErrCnt}=PingState,
    %?LOG('$trace',"~p. handle_ping_result. Node:~120p; PState:~120p; ErrCnt:~120p",[?MLogPrefix,Node,PState,ErrCnt]),
    #ping_state{state=PState1}=PingState1 =
        case PingResult of
            ok -> PingState#ping_state{state=ok,error_cnt=0,last_ts=?BU:timestamp()};
            {error,_} -> PingState#ping_state{state=error,error_cnt=ErrCnt+1}
        end,
    %?LOG('$trace',"~p. handle_ping_result. Node:~120p; new PState:~120p",[?MLogPrefix,Node,PState1]),
    {ok,NodeRec#node{ping_state=PingState1}}.

%% ------------------
%% Restart node if need
%% ------------------

%% ---
restart_node(#node{node=Node,ping_state=#ping_state{state=ok}}) ->
    ?LOG('$trace',"~p. restart_node. Node:~120p restart not needed, PState is ok",[?MLogPrefix,Node]),
    ok;
restart_node(#node{node=Node}=NodeRec) ->
    ?LOG('$trace',"~p. restart_node. Node:~120p init stop old node.",[?MLogPrefix,Node]),
    ok = stop_old_node(NodeRec),
    ?LOG('$trace',"~p. restart_node. Node:~120p init start node.",[?MLogPrefix,Node]),
    start_node(NodeRec).

%% ------------------
%% Resume node after stopping (server was suspended)
%% ------------------

%% ---
resume_node(NodeRec) ->
    #node{node=Node,name=NodeName}=NodeRec,
    ?LOG('$trace',"~p. resume_node. Node:~120p",[?MLogPrefix,Node]),
    ?LOG('$info',"~p. resume_node. Resume NodeName (~120p)",[?MLogPrefix,NodeName]),
    NewNodeRec = NodeRec#node{state=?NStW},
    {ok,NewNodeRec}.