%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 08.03.2021 15:09
%%% @doc

-module(cfgshell_master_fsm).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([handle_event/2, handle_event/3,
         handle_node_event/2,
         %
         initing/3,
         loading/3,
         working/3,
         suspending/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(ReloadTout, 10000).
-define(ReloadNowTout, 100).
-define(MLogPrefix, "Master FSM").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec handle_event({EventType::?ME, Event::term()}, State::#msrvstate{}) -> State1::#msrvstate{}.
%% ---
handle_event({?ME, Event}, #msrvstate{state=St}=State) ->
    handle_event({?ME, Event}, #{}, #msrvstate{state=St}=State).

%% ---
-spec handle_event({EventType::?ME, Event::term()}, MapOpts::map(), State::#msrvstate{}) -> State1::#msrvstate{}.
%% ---
handle_event({?ME, Event}, MapOpts, #msrvstate{state=St}=State) ->
    ?LOG('$trace',"~p. <-- Event (~120p) state before: ~120p",[?MLogPrefix,Event,St]),
    #msrvstate{state=St1}=State1 = ?MODULE:St(Event,MapOpts,State),
    ?LOG('$trace',"~p. --> Event (~120p) state after: ~120p",[?MLogPrefix,Event,St1]),
    State1.

%% ---
-spec handle_node_event({EventType::?NE, Event::term(), NodeName::binary()}, State::#msrvstate{}) -> ok.
%% ---
handle_node_event({?NE,Event,NodeName}, #msrvstate{nodes_ets=NodesEts}=State) ->
    ?LOG('$trace',"~p. Node Event (~120p) for node:~120p",[?MLogPrefix,Event,NodeName]),
    case find_node(NodeName,NodesEts) of
        {error,_Err} ->
            ?LOG('$trace',"~p. Node Event (~120p) find node:~120p error:~120p",[?MLogPrefix,NodeName,_Err]),
            ok;
        {ok,NodeRec} ->
            case ?MNFsm:handle_event(Event,NodeRec,State) of
                ok -> ok;
                {ok,NewNodeRec} when NewNodeRec==NodeRec -> ok;
                {ok,NewNodeRec} ->
                    save_noderec(NewNodeRec,State);
                {error,NewNodeRec,Reason} ->
                    ?LOG('$trace',"~p. Node Event (~120p) handle node:120p event error:~120p",[?MLogPrefix,NodeName,Reason]),
                    save_noderec(NewNodeRec,State)
            end,
            ok
    end.

%% ====================================================================
%% FSM
%% ====================================================================

%% ------------------
%% initing
%% ------------------

initing(load_cfg, _, #msrvstate{workers=#msworkers{cfg_load=undefined}}=State) ->
    State1 = do_load_cfg(State),
    State1#msrvstate{state=?MStL}.

%% ------------------
%% loading
%% ------------------

%% Load OK
loading({load_result,{ok,ResMap}}, _, State) ->
    #msrvstate{cfg_data=CfgD,workers=#msworkers{cfg_load={_Pid,_Ref}=LoadWorker}}=State,
    ?LOG('$trace',"~p. load_result. result ok. ResMap:~120p",[?MLogPrefix,ResMap]),
    CfgD1 = CfgD#{cfg_hash => erlang:phash2(?CfgL:get_curr_cfg()), % @todo использовать для определения стейта в который перейти после ошибки при применении конфига по команде, если конфиг ни разу не был загружен, то в лоадинг, если был, то в воркинг
                  load_res => ResMap},
    State1 = ?MU:set_worker(node_refresh,LoadWorker,State),
    State2 = ?MU:clear_worker(cfg_load,State1),
    State2#msrvstate{cfg_data=CfgD1};

%% Load Error
loading({load_result,{error,ErrMap}}, _, State) ->
    #msrvstate{cfg_data=CfgD,ref=Ref}=State,
    ?LOG('$trace',"~p. load_result. result error. ErrMap:~120p",[?MLogPrefix,ErrMap]),
    CfgD1 = CfgD#{load_res => ErrMap},
    ?MSrv:send_after({?ME,load_cfg,Ref},?ReloadTout),
    State1 = ?MU:clear_worker(cfg_load,State),
    State1#msrvstate{state=?MStW,cfg_data=CfgD1};

%%
loading({ping_node,NodeName}, _, State) ->
    #msrvstate{nodes_ets=NodesEts}=State,
    F = fun(NodeRec) -> ?MFsmU:ping_node(NodeRec,State) end,
    find_node_and_do(NodeName,NodesEts,F),
    State;

%% ignore ping result in loading state, continue pinging
loading({ping_node_result,NodeName,_PingResult}, _, State) ->
    #msrvstate{nodes_ets=NodesEts}=State,
    F = fun(NodeRec) -> ?MFsmU:ping_node_after(NodeRec,State) end,
    find_node_and_do(NodeName,NodesEts,F),
    State;

%%
loading({node_refresh_result,Result}, _, State) ->
    ?LOG('$trace',"~p. node_refresh_result. result:~120p",[?MLogPrefix,Result]),
    #msrvstate{map=M,ref=Ref}=State,
    %
    [send_cfg_to_zoomaster() || maps:get(send_cfg_to_zoomaster,M,false)],
    [?MSrv:send_after({?ME,load_cfg,Ref},?ReloadNowTout) || maps:get(repeat_load,M,false)],
    %
    M1 = maps:without([send_cfg_to_zoomaster,repeat_load],M),
    State1 = ?MU:clear_worker(node_refresh,State),
    State1#msrvstate{state=?MStW,map=M1};

%% @todo add proc_down on load_cfg

%% save flag to send cfg after success loading
loading({from_zoomaster,get_current_cfg}, _, State) ->
    ?LOG('$trace',"~p. from_zoomaster. get_current_cfg",[?MLogPrefix]),
    #msrvstate{map=M}=State,
    M1 = M#{send_cfg_to_zoomaster => true},
    State#msrvstate{map=M1};

%%
loading(load_cfg, _, State) ->
    ?LOG('$trace',"~p. load_cfg. load skipped, already in loading",[?MLogPrefix]),
    State1 = save_delayed_load(State),
    State1;

%% try to reload cfg by external command @todo rename reload_cfg -> saveload_cfg
loading(reload_cfg, MapOpts, State) ->
    From = maps:get(from,MapOpts),
    Reply = {error, <<"the configuration is being loaded. try again later">>},
    gen_server:reply(From, Reply),
    State;

%% default
loading(Event, _, #msrvstate{state=St}=State) ->
    ?LOG('$trace',"~p. Unknown event (~120p) in state: ~120p",[?MLogPrefix,Event,St]),
    State.

%% ------------------
%% working
%% ------------------

%% start load cfg
working(load_cfg, _, #msrvstate{workers=#msworkers{cfg_load=undefined}}=State) ->
    State1 = do_load_cfg(State),
    State1#msrvstate{state=?MStL};

%% try to reload cfg by external command
working(reload_cfg, MapOpts, State) ->
    From = maps:get(from,MapOpts),
    Reply =
        case ?MWorking:reload_cfg() of
            {ok, _Result}=Ok -> Ok;
            {error, _Reason}=Err -> Err
        end,
    catch gen_server:reply(From, Reply),
    State;

%%
working({ping_node,NodeName}, _, State) ->
    #msrvstate{nodes_ets=NodesEts}=State,
    F = fun(NodeRec) -> ?MFsmU:ping_node(NodeRec,State) end,
    find_node_and_do(NodeName,NodesEts,F),
    State;

%%
working({ping_node_result,NodeName,PingResult}, _, State) ->
    #msrvstate{nodes_ets=NodesEts}=State,
    F = fun(NodeRec) ->
                NewNodeRec = ?MWorking:analyze_ping_result(NodeRec,PingResult,State),
                save_noderec(NewNodeRec,State),
                ?MFsmU:ping_node_after(NodeRec,State)
        end,
    find_node_and_do(NodeName,NodesEts,F),
    State;

%%
working(start_svc_check, _, #msrvstate{workers=#msworkers{check_ping=undefined}}=State) ->
    ?LOG('$trace',"~p. start_svc_check. check_ping is undefined",[?MLogPrefix]),
    FSpawn = fun() -> ?MFsmU:check_svcnode_ping(State) end,
    ChPingWorker = erlang:spawn_monitor(FSpawn),
    State1 = ?MU:set_worker(check_ping,ChPingWorker,State),
    State1;
%% check_ping worker already started
working(start_svc_check, _, State) ->
    ?LOG('$trace',"~p. start_svc_check. check_ping worker already started",[?MLogPrefix]),
    State;

working({ping_svcnode_result,PingResult}, _, State) ->
    ?LOG('$trace',"~p. ping_svcnode_result. PingResult:~120p",[?MLogPrefix,PingResult]),
    FSpawn = fun() -> ?MWorking:analyze_svcnode_ping_result(PingResult,State) end,
    AnalyzeWorker = erlang:spawn_monitor(FSpawn),
    State1 = ?MU:set_worker(analyze_svcnode_ping,AnalyzeWorker,State),
    State2 = ?MU:clear_worker(check_ping,State1),
    State2;

working(stop_nodes_by_ping, _, State) ->
    #msrvstate{ref=Ref}=State,
    ?LOG('$trace',"~p. stop_nodes_by_ping",[?MLogPrefix]),
    ok = ?MWorking:stop_nodes_by_ping(State),
    ?MSrv:send_after({?ME,start_svc_check,Ref},5000),
    State#msrvstate{state=?MStS};

%% load worker down
working({proc_down,{'DOWN',Ref,process,Pid,Error}}, _, #msrvstate{workers=#msworkers{cfg_load={Pid,Ref}}}=State) ->
    ?LOG('$trace',"~p. proc_down. cfg_load. Pid:~120p; Error:~120p",[?MLogPrefix,Pid,Error]),
    erlang:send_after(5000, self(), {load_cfg,Ref}),
    State1 = ?MU:clear_worker(cfg_load,State),
    State1;

%% node_refresh worker down
working({proc_down,{'DOWN',Ref,process,Pid,Error}}, _, #msrvstate{workers=#msworkers{node_refresh={Pid,Ref}}}=State) ->
    ?LOG('$trace',"~p. proc_down. node_refresh. Pid:~120p; Error:~120p",[?MLogPrefix,Pid,Error]),
    State1 = ?MU:clear_worker(node_refresh,State),
    State1;

%% check_ping worker down
working({proc_down,{'DOWN',Ref,process,Pid,normal}}, _, #msrvstate{workers=#msworkers{check_ping={Pid,Ref}}}=State) ->
    ?LOG('$trace',"~p. proc_down. check_ping. Pid:~120p; normal",[?MLogPrefix,Pid]),
    State1 = ?MU:clear_worker(check_ping,State),
    State1;

%%
working({from_zoomaster,get_current_cfg}, _, State) ->
    send_cfg_to_zoomaster(),
    State;

%% default
working(Event, _, #msrvstate{state=St}=State) ->
    ?LOG('$trace',"~p. Unknown event (~120p) in state: ~120p",[?MLogPrefix,Event,St]),
    State.

%% ------------------
%% suspending
%% ------------------

%%
suspending(start_svc_check, _, #msrvstate{workers=#msworkers{check_ping=undefined}}=State) ->
    ?LOG('$trace',"~p. start_svc_check. check_ping is undefined",[?MLogPrefix]),
    FSpawn = fun() -> ?MFsmU:check_svcnode_ping(State) end,
    ChPingWorker = erlang:spawn_monitor(FSpawn),
    State1 = ?MU:set_worker(check_ping,ChPingWorker,State),
    State1;
%% check_ping worker already started
suspending(start_svc_check, _, State) ->
    ?LOG('$trace',"~p. start_svc_check. check_ping worker already started",[?MLogPrefix]),
    State;

%%
suspending({ping_svcnode_result,PingResult}, _, State) ->
    ?LOG('$trace',"~p. ping_svcnode_result. PingResult:~120p",[?MLogPrefix,PingResult]),
    State1 = ?MU:clear_worker(check_ping,State),
    #msrvstate{map=M,ref=Ref}=State1,
    case ?MSuspending:analyze_svcnode_ping_result(PingResult,State1) of
        pang -> State1;
        pong ->
            [?MSrv:send_after({?ME,load_cfg,Ref},?ReloadNowTout) || maps:get(repeat_load,M,false)],
            M1 = maps:without([repeat_load],M),
            State1#msrvstate{state=?MStW,map=M1}
    end;

%% if nodes was stopped when server load cfg
suspending({node_refresh_result,Result}, _, State) ->
    ?LOG('$trace',"~p. node_refresh_result. result:~120p",[?MLogPrefix,Result]),
    State1 = ?MU:clear_worker(node_refresh,State),
    State1;

%% if nodes was stopped when server load cfg, skip ping nodes
suspending({ping_node,NodeName}, _, State) ->
    ?LOG('$trace',"~p. ping_node. For node '~p' skipped",[?MLogPrefix,NodeName]),
    State;

%% if nodes was stopped when server load cfg, skip analyze ping result
suspending({ping_node_result,NodeName,_PingResult}, _, State) ->
    ?LOG('$trace',"~p. ping_node_result. For node '~p' analyze skipped",[?MLogPrefix,NodeName]),
    State;

%% try to reload cfg by external command
suspending(reload_cfg, MapOpts, State) ->
    From = maps:get(from,MapOpts),
    Reply = {error, <<"the server is suspended the configuration cannot be loaded">>},
    gen_server:reply(From, Reply),
    State;

%%
suspending(load_cfg, _, State) ->
    ?LOG('$trace',"~p. load_cfg. load skipped, already in loading",[?MLogPrefix]),
    State1 = save_delayed_load(State),
    State1;

%% default
suspending(Event, _, #msrvstate{state=St}=State) ->
    ?LOG('$trace',"~p. Unknown event (~120p) in state: ~120p",[?MLogPrefix,Event,St]),
    State.

%% ====================================================================
%% Common FSM functions
%% ====================================================================

%% ---
do_load_cfg(State) ->
    ?LOG('$trace',"~p. load_cfg. rcv load_cfg",[?MLogPrefix]),
    #msrvstate{cfg_data=CfgD}=State,
    FSpawn = fun() -> ?MLoading:do_load(State) end,
    LoadWorker = erlang:spawn_monitor(FSpawn),
    ?LOG('$trace',"~p. load_cfg. load cfg worker:~120p",[?MLogPrefix,LoadWorker]),
    CfgD1 = CfgD#{load_ts => os:timestamp()},
    State1 = ?MU:set_worker(cfg_load,LoadWorker,State),
    State1#msrvstate{cfg_data=CfgD1}.

%% ---
%% send current if zoomaster doesn't have a cfg
send_cfg_to_zoomaster() ->
    Cfg = ?CfgL:get_curr_cfg_raw(), % exactly raw cfg
    Request = {from_master, save_current_cfg, Cfg},
    ?MZSrv:ext_cast(Request),
    ok.

%% ---
save_delayed_load(State) ->
    #msrvstate{map=M}=State,
    M1 = M#{repeat_load => true},
    State#msrvstate{map=M1}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
-spec find_node(NodeName::binary(),NodeEts::atom()) -> {ok,NodeRec::#node{}} | {error,Err::binary()}.
%% ---
find_node(NodeName,NodeEts) ->
    case ets:lookup(NodeEts,NodeName) of
        [] -> {error,<<"not_found">>};
        [NodeRec] -> {ok,NodeRec}
    end.

%% ---
find_node_and_do(NodeName,NodeEts,F) ->
    FWork = fun() ->
                    case find_node(NodeName,NodeEts) of
                        {ok,NodeRec} -> F(NodeRec);
                        {error,_}=Err -> Err
                    end
            end,
    ?BWSrv:cast_workf(NodeName, FWork).

%% ---
save_noderec(NewNodeRec,#msrvstate{nodes_ets=NodesEts}) ->
    ets:insert(NodesEts,NewNodeRec).