%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 13.02.2021 17:58
%%% @doc

-module(cfgshell_master_fsm_working).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([analyze_ping_result/3,
         analyze_svcnode_ping_result/2,
         stop_nodes_by_ping/1,
         %
         reload_cfg/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "Master FSM working").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec analyze_ping_result(NodeRec::#node{}, PingResult:: ok | {error,Reason::binary()}, State::#msrvstate{}) -> NewNodeRec::#node{}.
%% ---
analyze_ping_result(NodeRec,PingResult,#msrvstate{ref=Ref}) ->
    Node = ?MNFsm:parse_noderec(node,NodeRec),
    %?LOG('$trace',"~p. analyze_ping_result. Node:~120p; PingResult:~120p",[?MLogPrefix,Node,PingResult]),
    {ok,NewNodeRec} = ?MNFsm:handle_event({ping_result,PingResult},NodeRec),
    PingState = ?MNFsm:parse_noderec(ping_state,NewNodeRec),
    case analyze_ping_state(PingState) of
        ok -> ok;
        need_check ->
            ?LOG('$trace',"~p. analyze_ping_result. Node:~120p need_check",[?MLogPrefix,Node]),
            ?MSrv:cast({?ME,start_svc_check,Ref})
    end,
    NewNodeRec.

%% @private
analyze_ping_state(ok) -> ok;
analyze_ping_state({error,ErrCnt}) when ErrCnt>=3 -> need_check;
analyze_ping_state({error,_ErrCnt}) -> ok.

%% ---
-spec analyze_svcnode_ping_result(PingResult::pong | pang, State::#msrvstate{}) -> ok.
%% ---
analyze_svcnode_ping_result(pang,State) ->
    #msrvstate{ref=Ref}=State,
    ?LOG('$trace',"~p. analyze_svcnode_ping_result. pang",[?MLogPrefix]),
    ?MSrv:cast({?ME,stop_nodes_by_ping,Ref}),
    ok;
analyze_svcnode_ping_result(pong,State) ->
    #msrvstate{nodes_ets=NodesEts}=State,
    ?LOG('$trace',"~p. analyze_svcnode_ping_result. pong",[?MLogPrefix]),
    Ffoldl = fun(NodeRec,_) ->
                     NodeName = ?MNFsm:parse_noderec(nodename,NodeRec),
                     ?LOG('$trace',"~p. analyze_svcnode_ping_result. cast to start node:~120p",[?MLogPrefix,NodeName]),
                     ?MSrv:cast({?NE,restart_by_ping,NodeName})
             end,
    ets:foldl(Ffoldl, #{}, NodesEts),
    ok.

%% ---
-spec stop_nodes_by_ping(State::#msrvstate{}) -> ok.
%% ---
stop_nodes_by_ping(State) ->
    #msrvstate{nodes_ets=NodesEts}=State,
    ?LOG('$trace',"~p. stop_nodes_by_ping.",[?MLogPrefix]),
    FStop = fun(NodeRec,Acc) ->
                    {ok,NewNodeRec} = ?MNFsm:handle_event(stop_node,NodeRec),
                    [NewNodeRec|Acc]
            end,
    NewNodeRecs = ets:foldl(FStop, [], NodesEts),
    ets:insert(NodesEts,NewNodeRecs),
    ok.

%% ---
-spec reload_cfg() -> {ok, Result::map()} | {error, Reason::map()}.
%% ---
reload_cfg() ->
    case read_validate_cfg() of
        {ok,CfgRaw,ValidateRes} ->
            case send_cfg_to_zoo(CfgRaw) of
                ok -> make_reload_cfg_result({ok,ValidateRes});
                {error,_ErrCmd,_Reason}=Err ->
                    make_reload_cfg_result(Err)
            end;
        {error,_ErrCmd,_Reason}=Err ->
            make_reload_cfg_result(Err)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
read_validate_cfg() ->
    CurrCfg = ?CfgL:get_curr_cfg(),
    case ?CfgU:read_cfg() of
        {ok,CfgRaw} ->
            case ?SCfgU:read_syscfg() of
                {ok,SysCfg} ->
                    case ?CfgL:validate_cfg_raw(CfgRaw,CurrCfg,SysCfg) of
                        {ok,ValidateRes} ->
                            {ok,CfgRaw,ValidateRes};
                        {error,ValidateErr} ->
                            {error,validate_error,ValidateErr}
                    end;
                {error,ReadSysCfgErr} ->
                    {error,read_syscfg,ReadSysCfgErr}
            end;
        {error,ReadCfgErr} ->
            {error,read_cfg,ReadCfgErr}
    end.

%% ---
send_cfg_to_zoo(CfgRaw) ->
    Req = {from_master,save_cfg,CfgRaw},
    case catch ?MZSrv:ext_call(Req,15000) of
        ok -> ok;
        {error,Reason} ->
            {error,call_zoo,Reason}
    end.

%% ---
make_reload_cfg_result({ok,ValidateRes}) ->
    Result = #{result => ok,
               info => <<"load cfg started">>,
               validate_info => ValidateRes},
    {ok,Result};
make_reload_cfg_result({error,ErrCommand,Reason}) ->
    Result = #{result => error,
               error_command => ErrCommand,
               reason => Reason},
    {error,Result}.