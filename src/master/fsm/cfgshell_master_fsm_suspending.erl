%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 28.03.2021 17:25
%%% @doc

-module(cfgshell_master_fsm_suspending).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([analyze_svcnode_ping_result/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "Master FSM suspending").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec analyze_svcnode_ping_result(PingResult::pong | pang, State::#msrvstate{}) -> pong | pang.
%% ---
analyze_svcnode_ping_result(pang,State) ->
    #msrvstate{ref=Ref}=State,
    ?LOG('$trace',"~p. analyze_svcnode_ping_result. pang",[?MLogPrefix]),
    ?MSrv:send_after({?ME,start_svc_check,Ref},5000),
    pang;
analyze_svcnode_ping_result(pong,State) ->
    ?LOG('$trace',"~p. analyze_svcnode_ping_result. pong",[?MLogPrefix]),
    reload_nodes(State),
    pong.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
%% предпочтительный кейс - сразу начать пинговать если потеря сети (которая вызвала suspend сервера), т.к. ноды могут быть еще живыми.
reload_nodes(State) ->
    #msrvstate{nodes_ets=NodesEts}=State,
    Ffoldl = fun(NodeRec,_) ->
                     NodeName = ?MNFsm:parse_noderec(nodename,NodeRec),
                     ?LOG('$trace',"~p. reload_nodes. init ping to node:~120p",[?MLogPrefix,NodeName]),
                     {ok,NewNodeRec} = ?MNFsm:handle_event(resume_node,NodeRec),
                     ets:insert(NodesEts,NewNodeRec),
                     ?MFsmU:ping_node_after(NewNodeRec,0,State)
             end,
    ets:foldl(Ffoldl, #{}, NodesEts),
    ok.
