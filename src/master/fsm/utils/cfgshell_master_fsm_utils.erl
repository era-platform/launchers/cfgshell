%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 13.02.2021 19:25
%%% @doc

-module(cfgshell_master_fsm_utils).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([ping_node_after/2, ping_node_after/3,
         ping_node/2,
         check_svcnode_ping/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "Master FSM utils").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec ping_node_after(NodeRec::#node{}, State::#msrvstate{}) -> ok.
%% ---
ping_node_after(NodeRec,State) ->
    Timeout = ?LAvgU:get_value_based_on_loadavg(ping_node_after_timeout),
    ping_node_after(NodeRec,Timeout,State).

%% ---
-spec ping_node_after(NodeRec::#node{}, Timeout::integer(), State::#msrvstate{}) -> ok.
%% ---
ping_node_after(NodeRec, Timeout, State) ->
    #msrvstate{ref=Ref}=State,
    NodeName = ?MNFsm:parse_noderec(nodename,NodeRec),
    Req = {?ME,{ping_node,NodeName},Ref},
    ?MSrv:send_after(Req,Timeout),
    ok.

%% ---
-spec ping_node(NodeRec::#node{},State::#msrvstate{}) -> ok.
%% ---
ping_node(NodeRec,State) ->
    #msrvstate{cfg_data=CfgD,ref=Ref}=State,
    CfgHash = maps:get(cfg_hash, CfgD),
    [NodeName,Node] = ?MNFsm:multiparse_noderec([nodename,node],NodeRec),
    {CallNodeTO, CallGenSrvTO} = ?LAvgU:get_value_based_on_loadavg(ping_node_timeouts),
    MapOpts = #{cfg_hash => CfgHash,
                timeout => CallGenSrvTO},
    Result =
        case ?BRpc:call(Node, ?APPCfgStarter, service_ping, [MapOpts], CallNodeTO) of
            ok -> ok;
            {badrpc, Reason} -> {error,Reason}
        end,
    ?MSrv:cast({?ME,{ping_node_result,NodeName,Result},Ref}),
    ok.

%% ---
-spec check_svcnode_ping(State::#msrvstate{}) -> ok.
%% ---
check_svcnode_ping(#msrvstate{ref=Ref}) ->
    SvcNode = ?MU:get_svcnode_fullname(),
    PingResult =
        case start_check_node(SvcNode) of
            {0,_} -> ping_svc_node(SvcNode);
            {_,_} -> pang
        end,
    stop_check_node(SvcNode),
    ?MSrv:cast({?ME,{ping_svcnode_result,PingResult},Ref}).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
start_check_node(SvcNode) ->
    Cmd = ?BU:to_binary(?MU:get_svc_ping_node_start_cmd(SvcNode)),
    PipePath = ?LCfgU:srvpath(nodepipedir,SvcNode),
    NodeWorkdir = ?LCfgU:srvpath(nodewd,SvcNode),
    ?U:ensure_dir(PipePath),
    ?U:ensure_dir(NodeWorkdir),
    CmdRes = ?BU:cmd_exec(Cmd),
    ?LOG('$trace',"~p. start_check_node. node:~120p; result:~120p~ncmd:~120p",[?MLogPrefix,SvcNode,CmdRes,Cmd]),
    CmdRes.

%% ---
stop_check_node(SvcNode) ->
    timer:sleep(200),
    Cmd = "ps -ef | awk '[-]name "++?BU:to_list(SvcNode)++"@/ {print $0}' | grep -v awk | awk '{print $2}'",
    ProcList = string:tokens(?BU:os_cmd(Cmd),"\n"),
    [?BU:os_cmd("kill "++ ?BU:to_list(SystemPid)) || SystemPid <- ProcList].

%% ---
ping_svc_node(SvcNode) ->
    do_ping_svc_node(SvcNode,0,pang).

%% @private
do_ping_svc_node(_,CheckCnt,Res) when CheckCnt > 7 -> Res;
do_ping_svc_node(SvcMsvcNodeName,Cnt,_) ->
    PingRes = net_adm:ping(SvcMsvcNodeName),
    ?OUTL("PING RES: ~120p",[PingRes]),
    case PingRes of
        pong -> pong;
        pang ->
            Sleep = svc_ping_sleep_tout(Cnt),
            timer:sleep(Sleep),
            do_ping_svc_node(SvcMsvcNodeName,Cnt+1,pang)
    end.

%% @private
svc_ping_sleep_tout(0) -> 100;
svc_ping_sleep_tout(1) -> 200;
svc_ping_sleep_tout(2) -> 400;
svc_ping_sleep_tout(3) -> 800;
svc_ping_sleep_tout(4) -> 1300;
svc_ping_sleep_tout(5) -> 2000;
svc_ping_sleep_tout(_) -> 3000.