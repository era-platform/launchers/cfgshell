%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 29.07.2021 3:33
%%% @doc

-module(cfgshell_master_fsm_loading).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([do_load/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").
-include_lib("stdlib/include/ms_transform.hrl").

-define(MsvcsPackSize, 4).
-define(MsvcsPackStartSleep, 2000).
-define(MLogPrefix, "Master FSM loading").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec do_load(State::#msrvstate{}) -> ok.
%% ---
do_load(State) ->
    #msrvstate{ref=Ref}=State,
    ?LOG('$trace',"~p. do_load.",[?MLogPrefix]),
    case ?CfgU:load_cfg() of
        {ok,_ResMap}=Ok ->
            ?LOG('$trace',"~p. do_load. load_cfg ok",[?MLogPrefix]),
            ?MSrv:cast({?ME,{load_result,Ok},Ref}),
            ok = prepare_server(),
            RefreshNodesRes = refresh_nodes(State),
            ?MSrv:cast({?ME,{node_refresh_result,RefreshNodesRes},Ref});
        {error,_ErrMap}=Err ->
            ?LOG('$trace',"~p. do_load. load_cfg error:~120p",[?MLogPrefix,_ErrMap]),
            ?MSrv:cast({?ME,{load_result,Err},Ref})
    end,
    ok.

    %% ====================================================================
    %% Internal functions
    %% ====================================================================

    %% ---
    -spec prepare_server() -> ok.
%% ---
%% prepare server after load cfg (create srv paths, ...).
prepare_server() ->
    ?LOG('$trace',"~p. prepare_server.",[?MLogPrefix]),
    ok.

%% ---
-spec refresh_nodes(State::#msrvstate{}) -> ok.
%% ---
%% create node records and sync with current nodes in ets
refresh_nodes(State) ->
    ?LOG('$trace',"~p. refresh_nodes.",[?MLogPrefix]),
    #msrvstate{nodes_ets=NodesEts}=State,
    EtsNodeData = get_ets_nodedata(NodesEts),
    CurrSrvNodesData = get_current_srv_nodesdata(),
    {DelNodesData,AddNodesData,UpdateNodesData}=Changes = get_nodes_changes(EtsNodeData,CurrSrvNodesData),
    log_nodes_changes(Changes),
    ?LOG('$trace',"~p. refresh_nodes.~nAddNodesData:~120p",[?MLogPrefix,AddNodesData]),
    ?LOG('$trace',"~p. refresh_nodes.~nDelNodesData:~n~120p",[?MLogPrefix,DelNodesData]),
    ?LOG('$trace',"~p. refresh_nodes.~nUpdateNodesData:~n~120p",[?MLogPrefix,UpdateNodesData]),
    ok = init_nodes(AddNodesData,NodesEts),
    ok = stop_nodes(DelNodesData,NodesEts),
    ok = update_nodes(UpdateNodesData,NodesEts),
    StartNodesRes = start_nodes(AddNodesData,State),
    ?LOG('$trace',"~p. refresh_nodes.StartNodesRes:~n~120p",[?MLogPrefix,StartNodesRes]),
    StartNodesRes.

%% @private
log_nodes_changes({DelNodesData,AddNodesData,UpdateNodesData}) ->
    F = fun(NodesData) -> [maps:get(<<"name">>,NodeData) || NodeData <- NodesData] end,
    ?LOG('$info',"~p. delete nodes: ~120p",[?MLogPrefix,F(DelNodesData)]),
    ?LOG('$info',"~p. update nodes: ~120p",[?MLogPrefix,F(UpdateNodesData)]),
    ?LOG('$info',"~p. add nodes: ~120p",[?MLogPrefix,F(AddNodesData)]),
    ok.

%% @private
get_ets_nodedata(NodesEts) ->
    ?LOG('$trace',"~p. get_ets_msvcdata.",[?MLogPrefix]),
    FMs = ets:fun2ms(fun(#node{msvc_data=MsvcData}) -> MsvcData end),
    ets:select(NodesEts,FMs).

%% @private
get_current_srv_nodesdata() ->
    ?LOG('$trace',"~p. get_current_srv_msvcdata.",[?MLogPrefix]),
    SrvNodeName = ?U:nodename(),
    MsvcFilters = [{fldeq,[{<<"server">>,SrvNodeName}]}],
    ?CfgLF:get_nodes_data(MsvcFilters).

%% @private
get_nodes_changes(OldNodeData,NewNodeData) ->
    NewNodeNames = [maps:get(<<"name">>,M) || M <- NewNodeData],
    OldNodeNames = [maps:get(<<"name">>,M) || M <- OldNodeData],
    %
    DelNodeNames = OldNodeNames -- NewNodeNames,
    AddNodeNames = NewNodeNames -- OldNodeNames,
    %
    CheckToUpdateNodeNames = ?U:get_intersect(NewNodeNames,OldNodeNames),
    FunCheckNewNodeNames = fun(CheckUpNodeName,Acc) ->
                                   [NewNodeMap] = [M || M <- NewNodeData, maps:get(<<"name">>,M) == CheckUpNodeName],
                                   [OldNodeMap] = [M || M <- OldNodeData, maps:get(<<"name">>,M) == CheckUpNodeName],
                                   case clear_nodedata(NewNodeMap) == clear_nodedata(OldNodeMap) of
                                       true -> Acc;
                                       false -> [NewNodeMap|Acc]
                                   end
                           end,
    UpdateNodeData = lists:foldl(FunCheckNewNodeNames, [], CheckToUpdateNodeNames),
    AddNodeData = [M || M <- NewNodeData, lists:member(maps:get(<<"name">>,M),AddNodeNames)],
    DelNodeData = [M || M <- OldNodeData, lists:member(maps:get(<<"name">>,M),DelNodeNames)],
    {DelNodeData,AddNodeData,UpdateNodeData}.

%% @private
clear_nodedata(NodeData) when is_map(NodeData) ->
    ChangeableFields = [<<"node_order">>,<<"structure_order">>],
    maps:without(ChangeableFields,NodeData).

%% @private
init_nodes(AddNodesData,NodesEts) ->
    ?LOG('$trace',"~p. init_nodes.",[?MLogPrefix]),
    FInit = fun(NodeData) ->
                    NodeRec = ?MNFsm:init_node(NodeData),
                    ets:insert(NodesEts,NodeRec)
            end,
    [FInit(XNodeData) || XNodeData <- AddNodesData],
    ok.

%% @private
stop_nodes(DelNodesData,NodesEts) ->
    ?LOG('$trace',"~p. stop_nodes.",[?MLogPrefix]),
    FStop = fun(NodeData) ->
                    NodeName = maps:get(<<"name">>,NodeData),
                    case ets:lookup(NodesEts,NodeName) of
                        [] -> ok;
                        [NodeRec] ->
                            NodeName = ?MNFsm:parse_noderec(nodename,NodeRec),
                            {ok,_NodeRec} = ?MNFsm:handle_event(stop_node,NodeRec),
                            ets:delete(NodesEts,NodeName)
                    end end,
    lists:foreach(FStop, DelNodesData),
    ok.

%% @private
update_nodes(UpdNodesData,NodesEts) ->
    ?LOG('$trace',"~p. update_nodes.",[?MLogPrefix]),
    FUpd = fun(NodeData) ->
                   NodeName = maps:get(<<"name">>,NodeData),
                   ?LOG('$trace',"~p. update_nodes. NodeName:~120p",[?MLogPrefix,NodeName]),
                   case ets:lookup(NodesEts,NodeName) of
                       [] -> ok;
                       [OldNodeRec] ->
                           {ok,NewNodeRec} = ?MNFsm:handle_event({update_node,NodeData},OldNodeRec), % @TODO add case of {ok,newnoderec} or restart
                           ets:insert(NodesEts,NewNodeRec)
                   end end,
    lists:foreach(FUpd, UpdNodesData),
    ok.

%% @private
start_nodes(NodesData, State) ->
    ?LOG('$trace',"~p. start_nodes.",[?MLogPrefix]),
    SortedNodes = sort_nodes(NodesData),
    SplitedNodes = split_nodes(SortedNodes,[]),
    ?LOG('$info',"~p. start_nodes.~nNodes to start:~120p",[?MLogPrefix,SplitedNodes]),
    start_grouped_nodes(SplitedNodes,State).

%% @private
sort_nodes(NodesData) ->
    ?LOG('$trace',"~p. sort_nodes.",[?MLogPrefix]),
    FSort = fun(NodeItem1,NodeItem2) ->
                    NodeIOrder1 = maps:get(<<"structure_order">>,NodeItem1),
                    NodeIOrder2 = maps:get(<<"structure_order">>,NodeItem2),
                    NodeIOrder1 =< NodeIOrder2
            end,
    lists:sort(FSort,NodesData).

%% @private
split_nodes([],Acc) -> Acc;
split_nodes(NodesData,Acc) when length(NodesData) =< ?MsvcsPackSize -> Acc ++ [NodesData];
split_nodes(NodesData,Acc) ->
    {NodesPack,Tail} = lists:split(?MsvcsPackSize,NodesData),
    split_nodes(Tail,Acc++[NodesPack]).

%% @private
start_grouped_nodes([],_) -> ok;
start_grouped_nodes([NodesDataPack|T],State) ->
    ?LOG('$trace',"~p. start_grouped_nodes. Start pack",[?MLogPrefix]),
    #msrvstate{nodes_ets=NodesEts}=State,
    FStart = fun(NodeData) ->
                     NodeName = maps:get(<<"name">>,NodeData),
                     ?LOG('$trace',"~p. start_grouped_nodes. Start NodeName:~120p",[?MLogPrefix,NodeName]),
                     [NodeRec] = ets:lookup(NodesEts,NodeName),
                     ok = ?MNFsm:handle_event(check_old_node_is_stopped,NodeRec),
                     NodeRecX =
                         case ?MNFsm:handle_event(start_node,NodeRec) of
                             {ok,NodeRec2} ->
                                 NodeRec2;
                             {error,NodeRec3,Reason} ->
                                 ?LOG('$trace',"~p. start_grouped_nodes. Start NodeName (~120p) error: ~120p",[?MLogPrefix,NodeName,Reason]),
                                 NodeRec3
                         end,
                     ets:insert(NodesEts,NodeRecX), % при любом исходе запуска всё равно добавляем в етс
                     ?MFsmU:ping_node_after(NodeRec,0,State) % при любом исходе запуска всё равно запускаем пинг, т.к. конфиг уже загружен и за счет пингов будут происходить попытки перезапуска
             end,
    lists:foreach(FStart,NodesDataPack),
    timer:sleep(?MsvcsPackStartSleep),
    start_grouped_nodes(T,State).