%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 17.05.2021 16:25
%%% @doc

-module(cfgshell_installer_commands_load_cfg).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([do_command/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "Load_cfg").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec do_command(MapOpts::map()) -> ok | {ok,Message::string()} | {error,ErrMsg::string()}.
%% ---
do_command(MapOpts) ->
    Command = reload_cfg,
    Timeout = 20000,
    CallMOpts = #{timeout => Timeout},
    MFA = {?APP,installer_request,[Command,CallMOpts]},
    handle_response(?InstallerU:call_cfgshell_server(MFA,Timeout,MapOpts)).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
handle_response({ok,_}=Ok) -> Ok;
handle_response({error,_}=Err) -> Err;
handle_response({badrpc, _Reason}) ->
    ?OUTL("~p. get badrpc reason: ~120p",[?MLogPrefix,_Reason]),
    {error,<<"call servernode error">>}.