%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 02.01.2021 17:55
%%% @doc

-module(cfgshell_installer_commands_install).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([do_command/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(DefSrvType, <<"default">>).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec do_command(MapOpts::map()) -> ok | {ok,Message::string()} | {error,ErrMsg::string()}.
%% ---
do_command(MapOpts0) ->
    case check_and_build_opts(MapOpts0) of
        {ok,MapOpts} ->
            Module = maps:get(module,MapOpts),
            Module:do_command(MapOpts);
        {error,_}=Err -> Err
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
check_and_build_opts(MapOpts) ->
    case check_and_build_opts_1(MapOpts) of
        {ok,MapOpts1} -> add_internal_opts(MapOpts1);
        {error,_}=Err -> Err
    end.

%% servertype
check_and_build_opts_1(MapOpts) ->
    AllowedInstallTypes = [<<"dev">>,?DefSrvType],
    ServerType = maps:get(servertype,MapOpts,?DefSrvType),
    case lists:member(ServerType,AllowedInstallTypes) of
        true ->
            MapOpts1 = MapOpts#{module => get_install_handler_module(ServerType)},
            check_and_build_opts_2(MapOpts1);
        false ->
            {error,<<"Invalid install servertype">>}
    end.

%% @private
get_install_handler_module(<<"dev">>) -> ?CmdInstDevM;
get_install_handler_module(?DefSrvType) -> ?CmdInstDefM.

%% srvname
check_and_build_opts_2(MapOpts) ->
    case maps:get(srvname,MapOpts,undef) of
        undef -> {error,<<"'srvname' is missed">>};
        _Val -> check_and_build_opts_3(MapOpts) % @TODO check is name valid
    end.

%% srvname
check_and_build_opts_3(MapOpts) ->
    case maps:get(srvhost,MapOpts,undef) of
        undef -> {error,<<"'srvhost' is missed">>};
        _Val -> check_and_build_opts_4(MapOpts) % @TODO check is host valid
    end.

%% psk
check_and_build_opts_4(MapOpts) ->
    case maps:get(psk,MapOpts,undef) of
        undef -> {error,<<"'psk' is missed">>};
        _Val -> check_and_build_opts_5(MapOpts) % @TODO check is psk valid
    end.

%% distports for cfgshell node (mindistport,maxdistport)
check_and_build_opts_5(MapOpts) ->
    FAdd = fun(MOpts,Min,Max) -> MOpts#{mindistport => Min, maxdistport => Max} end,
    case maps:get(distports,MapOpts,undef) of
        undef ->
            MapOpts1 = FAdd(MapOpts,?InstallerLCfg:def_srv_port_range(min),?InstallerLCfg:def_srv_port_range(max)),
            check_and_build_opts_6(MapOpts1);
        DPorts ->
            case binary:split(DPorts,<<":">>,[trim_all,global]) of
                [From,Cnt] -> % @TODO check from and cnt is valid
                    {FromInt,CntInt} = {?BU:to_int(From),?BU:to_int(Cnt)},
                    MapOpts1 = FAdd(MapOpts,From,?BU:to_binary(FromInt+CntInt-1)),
                    check_and_build_opts_6(MapOpts1);
                _ -> {error,<<"'distports' is invalid">>}
            end
    end.

%% loglevel
check_and_build_opts_6(MapOpts) ->
    case maps:get(loglevel,MapOpts,undef) of
        undef -> check_and_build_opts_x(MapOpts);
        LogLevel ->
            AllowedLvls = [<<"trace">>,<<"info">>],
            case lists:member(LogLevel,AllowedLvls) of
                true ->
                    MOpts1 = MapOpts#{loglevel:= <<"\$",LogLevel/binary>>},
                    check_and_build_opts_x(MOpts1);
                false -> {error, <<"'loglevel' is invalid. allowed values: 'trace', 'info'">>}
            end
    end.

%% check opts in install type module. always on end
check_and_build_opts_x(MapOpts) ->
    M = maps:get(module,MapOpts),
    case M:check_and_build_opts(MapOpts) of
        {ok,MOpts} -> set_optional_values(MOpts);
        {error,_}=Err -> Err
    end.

%% optional opts
set_optional_values(MapOpts) ->
    AllowedKeys = [erl_rootdir,pipe_dir,log_dir,work_dir,config_dir,connection_string,loglevel], % @todo add check for keys in check and build
    Fdefault = fun(Key,Acc) ->
                       case maps:is_key(Key,Acc) of
                           true -> Acc;
                           false -> Acc#{Key => get_opts_default(Key,MapOpts)}
                       end end,
    MapOpts1 = lists:foldl(Fdefault, MapOpts, AllowedKeys),
    {ok,MapOpts1}.

%% ---
add_internal_opts(MapOpts) ->
    [SrvName,SrvHost,InstDir] = ?BU:maps_get([srvname,srvhost,installdir],MapOpts),
    ServerNodeName = <<SrvName/binary,"@",SrvHost/binary>>,
    CfgSrvEbin = ?BU:to_binary(filename:join([InstDir,"cfgshell","cfgshell","ebin"])),
    MapOptsX = MapOpts#{srvnodename => ServerNodeName,
                        cfgsrvebin => CfgSrvEbin},
    {ok,MapOptsX}.

%% ---
get_opts_default(erl_rootdir,MapOpts) ->
    InstallDir = ?BU:to_binary(maps:get(installdir,MapOpts)),
    ?InstallerLCfg:srvpath(install,InstallDir,erlroot);
get_opts_default(pipe_dir,_) -> ?InstallerLCfg:srvpath(pipedir);
get_opts_default(log_dir,_) -> ?InstallerLCfg:srvpath(varlog);
get_opts_default(work_dir,_) -> ?InstallerLCfg:srvpath(srvwd);
get_opts_default(config_dir,_) -> ?InstallerLCfg:srvpath(cfgsd);
get_opts_default(connection_string,_) -> undefined;
get_opts_default(loglevel,_) -> '$info'.