%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 13.05.2021 15:01
%%% @doc

-module(cfgshell_installer_commands_install_default).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([do_command/1,
         check_and_build_opts/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "Install def").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec do_command(MapOpts::map()) -> ok | {ok,Message::string()} | {error,ErrMsg::string()}.
%% ---
do_command(MapOpts) ->
    do_install_def_1(MapOpts).

%% ---
-spec check_and_build_opts(MapOpts::map()) -> {ok,NewMapOpts::map()} | {error,Reason::binary()}.
%% ---
check_and_build_opts(MapOpts) ->
    check_and_build_opts_1(MapOpts).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------
%% check_and_build_opts
%% ------------------------------

%% install dir
check_and_build_opts_1(MapOpts) ->
    DefInstDir = ?InstallerLCfg:srvpath(instdir),
    MapOpts1 = MapOpts#{installdir => ?BU:to_binary(DefInstDir)},
    check_and_build_opts_2(MapOpts1).

%% zookeeper connect
check_and_build_opts_2(MapOpts) ->
    case maps:get(connection_string,MapOpts,undef) of
        undef -> check_and_build_opts_x(MapOpts);
        ConnStr ->
            case check_connstr(ConnStr) of
                ok -> check_and_build_opts_x(MapOpts);
                {error,_}=Err -> Err
            end
    end.

%% always on end
check_and_build_opts_x(MapOpts) ->
    {ok,MapOpts}.

%% @private
check_connstr(ConnStr) -> ok.

%% ------------------------------
%% install in default(production) mode
%% ------------------------------

%% create bin script
do_install_def_1(MapOpts) ->
    InstallDir = maps:get(installdir,MapOpts),
    case ?CmdInstScr:create_bin_script(InstallDir) of
        ok -> do_install_def_2(MapOpts);
        {error,_}=Err -> Err
    end.

%% create installer script
do_install_def_2(MapOpts) ->
    case ?CmdInstScr:create_installer_script(MapOpts) of
        ok -> do_install_def_3(MapOpts);
        {error,_}=Err -> Err
    end.

%% create directories
do_install_def_3(MapOpts) ->
    ServerNodeName = maps:get(srvnodename,MapOpts),
    ServerWorkDir = filename:join([?InstallerLCfg:srvpath(srvwd),ServerNodeName]),
    Dirs = [ServerWorkDir, ?InstallerLCfg:srvpath(varlog), ?InstallerLCfg:srvpath(pipedir)],
    FEnsureDir = fun(_,{error,_}=Acc) -> Acc;
                    (Dir,ok) ->
                         case ?U:ensure_dir(Dir) of
                             ok -> ok;
                             {error,_}=Err -> Err % @TODO add print to log
                         end
                 end,
    case lists:foldl(FEnsureDir,ok,Dirs) of
        ok -> do_install_def_4(MapOpts);
        {error,_}=Err -> Err
    end.

%% create starter script
do_install_def_4(MapOpts) ->
    case ?CmdInstScr:create_starter_script(MapOpts) of
        ok -> do_install_def_5(MapOpts);
        {error,_}=Err -> Err
    end.

%% create local config (for app cfgstore)
do_install_def_5(MapOpts) ->
    LocalCfg = get_local_cfg_data(MapOpts),
    ConfigDir = maps:get(config_dir,MapOpts),
    ?OUTL("~p. do_install_def_5. ConfigDir:~120p",[?MLogPrefix,ConfigDir]),
    ?U:ensure_dir(ConfigDir),
    case ?U:create_json_file(ConfigDir,?LocalCfgFileName,LocalCfg) of
        ok -> do_install_def_6(MapOpts);
        {error,_}=Err -> Err
    end.

%% create site.cfg.json for first start
do_install_def_6(MapOpts) ->
    %% @todo implement create cfg if need
    do_install_dev_x(MapOpts).

%% always on end
do_install_dev_x(_MapOpts) ->
    ok.

%% ------------------------------
%% local cfg
%% ------------------------------

get_local_cfg_data(MapOpts) ->
    CfgKeys = [srvname,srvhost,psk,connection_string,mindistport,maxdistport,
               erl_rootdir,pipe_dir,log_dir,work_dir,config_dir,loglevel],
    maps:with(CfgKeys,MapOpts).