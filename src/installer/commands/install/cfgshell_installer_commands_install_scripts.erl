%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 04.01.2021 16:10
%%% @doc

-module(cfgshell_installer_commands_install_scripts).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([create_bin_script/1,
         create_installer_script/1,
         create_starter_script/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "Install utils").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec create_bin_script(InstallDir::binary()) -> ok | {error,Error::term()}.
%% ---
create_bin_script(InstallDir) ->
    InstallPathBin = ?BU:to_binary(filename:join([InstallDir,?InstallerLCfg:installer_sh_filename()])),
    BinContent = get_bin_content(InstallPathBin),
    BinPath = ?InstallerLCfg:srvpath(erabin),
    Res = file:write_file(BinPath,BinContent),
    file:change_mode(BinPath,8#700),
    ?OUTL("~p. Create bin script on path (~s) result: ~120p",[?MLogPrefix,BinPath,Res]),
    Res.

%% @private
get_bin_content(InstallPathBin) ->
    <<"#!/bin/bash

      export LANGUAGE=en_US:en
      export LANG=en_US.UTF-8
      export TERM=xterm

      shfilepath=",InstallPathBin/binary,"

      if [ -f $shfilepath ]
         then
         setsid -w $shfilepath $* | tee -a /tmp/installer_bin.log
         else
         echo \"installer script not found. Try exec 'run' command first.\"
    exit 1
    fi">>.

%% ---
-spec create_installer_script(MapOpts::map()) -> ok | {error,Error::term()}.
%% ---
create_installer_script(MapOpts) ->
    [InstallDir,CfgDir] = ?BU:maps_get([installdir,config_dir],MapOpts),
    case ?U:ensure_dir(InstallDir) of
        ok ->
            LocalCfgPath = ?BU:to_binary(filename:join([CfgDir,?LocalCfgFileNameWExt])),
            InstallPathBin = ?BU:to_binary(filename:join([InstallDir,?InstallerLCfg:installer_sh_filename()])),
            InstallerContent = get_installer_content(LocalCfgPath),
            Res = file:write_file(InstallPathBin,InstallerContent),
            file:change_mode(InstallPathBin,8#700),
            ?OUTL("~p. Create installer script on path (~s) result: ~120p",[?MLogPrefix,InstallPathBin,Res]),
            Res;
        {error,_}=Err -> Err
    end.

%% @private
get_installer_content(LocalCfgPath) ->
    RelativeEscriptDir = ?BU:to_binary(filename:join(["cfgshell","cfgshell","priv"])),
    EscriptFileName = <<"installer.escript">>,
    <<"#!/bin/bash\n
      
      local_cfgpath=",LocalCfgPath/binary,"
      erlang_bindir=$(jq -r '.erl_rootdir' $local_cfgpath)/bin
      scriptdir=$( cd \"$( dirname \"$0\" )\" && pwd -P)
      escriptfilepath=$(cd $scriptdir/",RelativeEscriptDir/binary," && pwd -P)/",EscriptFileName/binary,"

      export LANGUAGE=en_US:en
      export LANG=en_US.UTF-8
      export TERM=xterm

      export PATH=$erlang_bindir:$PATH
      
      dynnodename=$(date +\"%s%3N\")
      ERL_FLAGS=\"-name $dynnodename@localhost\"
      export ERL_FLAGS

      setsid -w $erlang_bindir/escript $escriptfilepath $* local_cfgpath=$local_cfgpath | tee -a /tmp/installer.log">>.

%% ---
-spec create_starter_script(MapOpts::map()) -> ok | {error,Error::term()}.
%% ---
create_starter_script(MapOpts) ->
    case ?U:ensure_dir(?InstallerLCfg:srvpath(erastarter)) of
        ok ->
            StarterPath = ?InstallerLCfg:srvpath(erastartsh),
            StarterContent = get_starter_content(MapOpts),
            Res = file:write_file(StarterPath,StarterContent),
            file:change_mode(StarterPath,8#755),
            ?OUTL("~p. Create starter script on path (~s) result: ~120p",[?MLogPrefix,StarterPath,Res]),
            Res;
        {error,_}=Err -> Err % @TODO add print to log
    end.

%% @private
get_starter_content(MapOpts) ->
    [CfgSrvEbinPath,CfgDir] = ?BU:maps_get([cfgsrvebin,config_dir],MapOpts),
    LocalCfgPath = filename:join([CfgDir,?LocalCfgFileNameWExt]),
    %
    <<"#!/bin/bash
       # Set HOME directory, need for boot erlexec
      export HOME=/root

      export LANGUAGE=en_US:en
      export LANG=en_US.UTF-8
      export TERM=xterm
      
      local_cfgpath=",LocalCfgPath/binary,"
      erlang_bindir=$(jq -r '.erl_rootdir' $local_cfgpath)/bin
      psk=$(jq -r '.psk' $local_cfgpath)
      
      srv_nodename=$(jq -r '.srvname' $local_cfgpath)@$(jq -r '.srvhost' $local_cfgpath)
      srv_pipedir=$(jq -r '.pipe_dir' $local_cfgpath)/$srv_nodename
      srv_workidr=$(jq -r '.work_dir' $local_cfgpath)/$srv_nodename
      srv_logdir=$(jq -r '.log_dir' $local_cfgpath)/$srv_nodename
      srv_mindistport=$(jq -r '.mindistport' $local_cfgpath)
      srv_maxdistport=$(jq -r '.maxdistport' $local_cfgpath)
      
      mkdir -p $srv_pipedir $srv_logdir
      
      if [[ -d $srv_workidr/log ]]; then rm -rf $srv_workidr/log; fi
      ln -fsT $srv_logdir $srv_workidr/log
      
      $erlang_bindir/run_erl $srv_pipedir/_erlang.pipe $srv_workidr",
      " \"cd $srv_workidr && $erlang_bindir/erl",
      " -pa ",CfgSrvEbinPath/binary,
      " -name $srv_nodename", %% @todo переосмыслить количество shell скриптов (/usr/bin/sh -> installer.sh -> installer.escript)
      " -run cfgshell",
      " -local_cfg ",LocalCfgPath/binary,
      " -setcookie $psk",
      " +pc unicode -hidden -shutdown_time 2000",
      " -kernel inet_dist_listen_min $srv_mindistport inet_dist_listen_max $srv_maxdistport\"">>.



%% ====================================================================
%% Internal functions
%% ====================================================================
