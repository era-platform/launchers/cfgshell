%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 04.01.2021 16:10
%%% @doc

-module(cfgshell_installer_commands_install_dev).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([do_command/1,
         check_and_build_opts/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec do_command(MapOpts::map()) -> ok | {ok,Message::string()} | {error,ErrMsg::string()}.
%% ---
do_command(MapOpts) ->
    do_install_dev_1(MapOpts).

%% ---
-spec check_and_build_opts(MapOpts::map()) -> {ok,NewMapOpts::map()} | {error,Reason::binary()}.
%% ---
check_and_build_opts(MapOpts) ->
    check_and_build_opts_1(MapOpts).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------
%% check_and_build_opts
%% ------------------------------

%% install dir
check_and_build_opts_1(MapOpts) ->
    MapOpts1 =
        case maps:get(installdir,MapOpts,undef) of
            undef ->
                DefInstDir = ?InstallerLCfg:srvpath(instdir),
                ?OUTL("Install. Install dir is undefined. Use default: ~120p",[DefInstDir]),
                MapOpts#{installdir => ?BU:to_binary(DefInstDir)};
            _ -> MapOpts % @TODO add check is path valid
        end,
    check_and_build_opts_x(MapOpts1).

%% always on end
check_and_build_opts_x(MapOpts) ->
    {ok,MapOpts}.

%% ------------------------------
%% install in develop mode
%% ------------------------------

%% create bin script
do_install_dev_1(MapOpts) ->
    InstallDir = maps:get(installdir,MapOpts),
    case ?CmdInstScr:create_bin_script(InstallDir) of
        ok -> do_install_dev_2(MapOpts);
        {error,_}=Err -> Err
    end.

%% create installer script
do_install_dev_2(MapOpts) ->
    case ?CmdInstScr:create_installer_script(MapOpts) of
        ok -> do_install_dev_3(MapOpts);
        {error,_}=Err -> Err
    end.

%% create directories
do_install_dev_3(MapOpts) ->
    ServerNodeName = maps:get(srvnodename,MapOpts),
    ServerWorkDir = filename:join([?InstallerLCfg:srvpath(srvwd),ServerNodeName]),
    Dirs = [ServerWorkDir, ?InstallerLCfg:srvpath(varlog), ?InstallerLCfg:srvpath(pipedir)],
    FEnsureDir = fun(_,{error,_}=Acc) -> Acc;
                    (Dir,ok) ->
                         case ?U:ensure_dir(Dir) of
                             ok -> ok;
                             {error,_}=Err -> Err % @TODO add print to log
                         end
                 end,
    case lists:foldl(FEnsureDir,ok,Dirs) of
        ok -> do_install_dev_4(MapOpts);
        {error,_}=Err -> Err
    end.

%% create starter script
do_install_dev_4(MapOpts) ->
    case ?CmdInstScr:create_starter_script(MapOpts) of
        ok -> do_install_dev_5(MapOpts);
        {error,_}=Err -> Err
    end.

%% create symlink from dev dir to install dir
do_install_dev_5(MapOpts) ->
    InstallDir = maps:get(installdir,MapOpts),
    do_install_dev_x(MapOpts).
%%    CodePath = ?InstallerLCfg:srvpath(srvmain),
%%    SymlinkPath = filename:join([InstallDir,"cfgshell"]),
%%    case ??CmdInstScr:ensure_symlink(CodePath,SymlinkPath) of
%%        ok -> do_install_dev_x(MapOpts);
%%        {error,_}=Err -> Err
%%    end.

%% always on end
do_install_dev_x(_MapOpts) ->
    ok.