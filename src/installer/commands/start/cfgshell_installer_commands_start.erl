%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 28.03.2021 19:11
%%% @doc

-module(cfgshell_installer_commands_start).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([do_command/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec do_command(MapOpts::map()) -> ok | {ok,Message::string()} | {error,ErrMsg::string()}.
%% ---
do_command(MapOpts0) ->
    case check_and_build_opts(MapOpts0) of
        {ok,MapOpts} ->
            StartType = maps:get(starttype,MapOpts,default),
            do_start_by_type(StartType,MapOpts);
        {error,_}=Err -> Err
    end.

%% @private
do_start_by_type(default,_) ->
    StartShPath = ?InstallerLCfg:srvpath(erastartsh),
    Cmd = <<"bash ",StartShPath/binary," 2>&1">>,
    ?OUTL("Start server with command: ~120p",[Cmd]),
    {Code,Msg} = ?BU:cmd_exec(Cmd),
    ?OUTL("Command stopped with code (~120p). Msg: ~120p",[Code,Msg]),
    format_stop_code(Code,Msg);
do_start_by_type(daemon,_) ->
    ok.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
check_and_build_opts(MapOpts) ->
    % @todo проверить нужны ли тут проверки
    {ok, MapOpts}.

%% ---
format_stop_code(0,Msg) -> {ok,Msg};
format_stop_code(Code,Msg) -> {error,{Code,Msg}}.
