%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 19.05.2021 14:39
%%% @doc

-module(cfgshell_installer_utils).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([call_cfgshell_server/3,
         get_localcfg/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "Installer utils").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec call_cfgshell_server({CallModule::atom(),CallFun::atom(),CallArgs::list()},Timeout::integer(),MapOpts::map()) ->
          {badrpc,Reason::term} | Result::term().
%% ---
call_cfgshell_server({M,F,A},Timeout,MapOpts) ->
    LocalCfgPath = maps:get(local_cfgpath,MapOpts),
    case ?InstallerU:get_localcfg(LocalCfgPath) of
        {ok,LCfg} ->
            case catch get_servernode(LCfg) of
                {'EXIT',_} -> {error,<<"missed required params in local config">>};
                SrvNode ->
                    set_servercookie(LCfg),
                    ?OUTL("~p. SrvNode: ~120p",[?MLogPrefix,SrvNode]),
                    ?BRpc:call(SrvNode,M,F,A,Timeout+200)
            end;
        {error,_}=Err2 -> Err2
    end.


%% ---
-spec get_localcfg(LocalCfgPath::binary()) -> {ok,LocalCfg::map()} | {error,Reason::binary()}.
%% ---
get_localcfg(LocalCfgPath) ->
    case file:read_file(LocalCfgPath) of
        {ok,Data} ->
            case catch jsx:decode(Data,[return_maps]) of
                {'EXIT',_Err} ->
                    ?OUTL("~p. _Err: ~120p",[?MLogPrefix,_Err]),
                    {error,<<"bad content in local config">>};
                LocalCfg -> {ok,LocalCfg}
            end;
        {error,_Err} -> {error,<<"read local config error">>}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
get_servernode(LCfg) ->
    [SrvName,SrvHost] = ?BU:maps_get([<<"srvname">>,<<"srvhost">>],LCfg),
    ?BU:to_atom_new(<<SrvName/binary,"@",SrvHost/binary>>).

%% ---
set_servercookie(LCfg) ->
    Psk = maps:get(<<"psk">>,LCfg),
    erlang:set_cookie(node(),?BU:to_atom_new(Psk)).