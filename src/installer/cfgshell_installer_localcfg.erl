%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 03.01.2021 19:28
%%% @doc

-module(cfgshell_installer_localcfg).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([installer_sh_filename/0,
         srvpath/1, srvpath/3,
         def_nodes_port_range/0, def_nodes_port_range/1,
         def_srv_port_range/0, def_srv_port_range/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").
-include_lib("kernel/include/file.hrl").

-define(DefSrvNodePortRange,{9300,10}).
-define(DefNodesPortRange,{9310,90}).

%% ====================================================================
%% API functions
%% ====================================================================

%% ---
installer_sh_filename() -> <<"installer.sh">>.

%% ---
-spec srvpath(Key::atom()) -> AbsPath::binary() | no_return().
%% ---
srvpath(Key) -> ?BU:to_binary(get_srv_path(Key)).

%% ---
-spec srvpath(Type::install, BaseDir::binary(), Key::atom()) -> AbsPath::binary() | no_return().
%% ---
srvpath(Type,BaseDir,Key) -> ?BU:to_binary(get_srv_path(Type,BaseDir,Key)).

%% ---
def_nodes_port_range() -> {Min,Cnt}=?DefNodesPortRange,?BU:to_binary(?BU:to_list(Min)++":"++?BU:to_list(Cnt)).
def_nodes_port_range(min) -> {Min,_}=?DefNodesPortRange,?BU:to_binary(Min);
def_nodes_port_range(max) -> {Min,Cnt}=?DefNodesPortRange,?BU:to_binary(Min+Cnt-1).

%% ---
def_srv_port_range() -> {Min,Cnt}=?DefNodesPortRange,?BU:to_binary(?BU:to_list(Min)++":"++?BU:to_list(Cnt)).
def_srv_port_range(min) -> {Min,_}=?DefNodesPortRange,?BU:to_binary(Min);
def_srv_port_range(max) -> {Min,Cnt}=?DefNodesPortRange,?BU:to_binary(Min+Cnt-1).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% main for all applications.
%% ex. /usr/lib/era
get_srv_path(main) ->
    MayBeSymLinkFolder = ?U:drop_last_subdir(code:which(?MODULE),4),
    io:format("Path:~120p~n",[MayBeSymLinkFolder]),
    get_mb_symlink_path(MayBeSymLinkFolder);
%% erlang executables
get_srv_path(erlbin) -> filename:join([get_srv_path(main),erl_dir_name(),"bin"]);
get_srv_path(runerl) -> filename:join([get_srv_path(erlbin),"run_erl"]);
get_srv_path(erl) -> filename:join([get_srv_path(erlbin),"erl"]);
get_srv_path(escript) -> filename:join([get_srv_path(erlbin),"escript"]);
%% main binary file
get_srv_path(erabin) -> filename:join(["/usr","bin","era"]);
%% default install folder
get_srv_path(instdir) -> filename:join(["/usr","lib","era"]);
%% runtime folder
get_srv_path(varlib) -> filename:join(["/var","lib","era"]);
%% runtime folder
get_srv_path(varlog) -> filename:join(["/var","log","era"]);
%% workdir folder
get_srv_path(srvwd) -> filename:join([get_srv_path(varlib),"_workdir"]);
get_srv_path(files) -> filename:join([get_srv_path(varlib),"files"]);
get_srv_path(nodewd) -> filename:join([get_srv_path(srvwd),?BU:to_list(node())]);
get_srv_path(cfgsd) -> filename:join([get_srv_path(varlib),"config"]);
%% installer folder
get_srv_path(erastarter) -> filename:join([get_srv_path(srvwd),"starter"]);
get_srv_path(erastartsh) -> filename:join([get_srv_path(erastarter),"start.sh"]);
%% erlang node pipes
get_srv_path(pipedir) -> filename:join(["/var","run","era"]).

%% ---
%% paths for installer
get_srv_path(install,BaseDir,erlroot) -> filename:join([BaseDir,erl_dir_name()]).

%% ---
get_mb_symlink_path(MayBeSymLinkFolder) ->
    case file:read_link_info(MayBeSymLinkFolder) of
        {ok,#file_info{type=directory}} -> MayBeSymLinkFolder;
        {ok,#file_info{type=symlink}} ->
            case file:read_link(MayBeSymLinkFolder) of
                {ok,LinkPath} -> LinkPath;
                {error,_}=Err -> exit(Err)
            end;
        {error,_}=Err -> exit(Err)
    end.

%% ---
erl_dir_name() -> <<"erlang">>.