%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 29.08.2021 17:52
%%% @doc

-module(cfgshell_procs_zoomaster_srv).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-behaviour(gen_server).

-export([start_link/1,
         %
         init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3,
         %
         send_after/2,
         cast/1, ext_cast/1,
         ext_call/2
        ]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("cfgshell.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, Opts, []).

%% ---
-spec send_after(Request::term(), Timeout::pos_integer()) -> ok.
%% ---
send_after(Request,Timeout) ->
    erlang:send_after(Timeout,?MODULE,Request),
    ok.

%% ---
-spec cast(Request::term()) -> ok.
%% ---
cast(Request) ->
    gen_server:cast(?MODULE,Request).

%% ---
-spec ext_cast(Request::term()) -> ok.
%% ---
ext_cast(Request) ->
    gen_server:cast(?MODULE,{?MZE, Request}).

%% ---
-spec ext_call(Request::term(),Timeout::integer()) -> Response::term().
%% ---
ext_call(Request,Timeout) ->
    gen_server:call(?MODULE,{?MZE, Request},Timeout).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Opts) ->
    Ref = erlang:make_ref(),
    State = #zoomsrvstate{state=?MZStI,
                          srv_pid = self(),
                          ref=Ref},
    erlang:send_after(0, self(), {?MZE, init, Ref}),
    ?LOG('$trace',"~p. inited",[?MODULE]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% external events
handle_call({?MZE, Event}, From, State) ->
    MapOpts = #{from => From},
    State1 = ?MZFsm:handle_event(Event, MapOpts, State),
    {noreply, State1};

handle_call(_Request, _From, State) ->
    ?LOG('$trace',"~p. rcv unknown CALL:~120p",[?MODULE,_Request]),
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({?MZE, Event, Ref}, #zoomsrvstate{ref=Ref}=State) ->
    State1 = ?MZFsm:handle_event(Event,State),
    {noreply, State1};

%% external events
handle_cast({?MZE, Event}, State) ->
    State1 = ?MZFsm:handle_event(Event,State),
    {noreply, State1};

%% Other
handle_cast(_Request, State) ->
    ?LOG('$trace',"~p. rcv unknown CAST:~120p",[?MODULE,_Request]),
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

handle_info({?MZE, Event, Ref}, #zoomsrvstate{ref=Ref}=State) ->
    State1 = ?MZFsm:handle_event(Event,State),
    {noreply, State1};

handle_info({'DOWN',_Ref,process,_Pid,_Error}=Req, State) ->
    State1 = ?MZFsm:handle_event({?MZE,{proc_down,Req}},State),
    {noreply, State1};

%% cfg changed in zookeeper
handle_info({node_data_changed, ?CfgZooNodePath}, State) ->
    Event = zoo_cfg_changed,
    State1 = ?MZFsm:handle_event(Event,State),
    {noreply, State1};

%% connection to zoo up
%% ex: {127,0,0,1}=Host, 2181=Port
handle_info({connected, _Host, _Port}, State) ->
    Event = zoo_connected,
    State1 = ?MZFsm:handle_event(Event,State),
    {noreply, State1};

%% connection to zoo down
handle_info({disconnected, _Host, _Port}, State) ->
    Event = zoo_disconnected,
    State1 = ?MZFsm:handle_event(Event,State),
    {noreply, State1};

%% connection to zoo expires
handle_info({expired,{127,0,0,1},2181}, State) ->
    % @todo перечитать документацию, возможно это никогда не наступит исходя из опций, реконект там автоматом, если так, то вырезать
    {noreply, State};

%% default
handle_info(_Info, State) ->
    ?LOG('$trace',"~p. rcv unknown INFO:~120p",[?MODULE,_Info]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================
