%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk  <llceceron@gmail.com>
%%% @date 04.09.2021 18:51
%%% @doc

-module(cfgshell_procs_zoomaster_fsm_working).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([get_cfg_from_zoo/1,
         save_cfg_to_local/1,
         save_cfg_to_zoo/2
        ]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "ZooMaster FSM. Working").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec get_cfg_from_zoo(State::#zoomsrvstate{}) -> {ok, empty | CfgRaw::binary()} | {error, Reason::atom()}.
%% ---
get_cfg_from_zoo(State) ->
    ?LOG('$trace',"~p. get_cfg_from_zoo",[?MLogPrefix]),
    #zoomsrvstate{srv_pid=SrvPid,conn_pid=ConnPid}=State,
    case ?AZK:get_data_subscr(ConnPid,?CfgZooNodePath,SrvPid) of
        {ok,<<>>} ->
            ?LOG('$trace',"~p. get_cfg_from_zoo. empty cfg in zoo",[?MLogPrefix]),
            {ok, empty};
        {ok,CfgData} ->
            {ok, CfgData};
        {error,Reason} ->
            ?LOG('$trace',"~p. get_cfg_from_zoo. get cfg from zoo error: ~120p",[?MLogPrefix,Reason]),
            {error,Reason}
    end.

%% ---
-spec save_cfg_to_local(CfgRaw::binary()) -> ok | {error, Reason::tuple()}.
%% ---
save_cfg_to_local(CfgRaw) ->
    ?LOG('$trace',"~p. save_cfg_to_local",[?MLogPrefix]),
    case checkwrite_cfg_to_disk(CfgRaw) of
        ok -> ok;
        {error,Reason}=Err ->
            ?LOG('$trace',"~p. save_cfg_to_local. write cfg to file error: ~120p",[?MLogPrefix,Reason]),
            Err
    end.

%% ---
-spec save_cfg_to_zoo(CfgMap::map(), State::#zoomsrvstate{}) -> ok | {error, Reason::atom()}.
%% ---
save_cfg_to_zoo(CfgMap, State) ->
    ?LOG('$trace',"~p. save_cfg_to_zoo",[?MLogPrefix]),
    #zoomsrvstate{conn_pid=ConnPid}=State,
    CfgRaw = jsx:encode(CfgMap,[space,{indent,2}]),
    case ?AZK:set_data(ConnPid,?CfgZooNodePath,CfgRaw) of
        ok -> ok;
        {error,Reason}=Err ->
            ?LOG('$trace',"~p. save_cfg_to_zoo. set_data error: ~120p",[?MLogPrefix,Reason]),
            Err
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
checkwrite_cfg_to_disk(NewCfgRaw) ->
    CurrCfg =
        case ?CfgU:read_cfg() of
            {ok,CurrCfgMap} -> CurrCfgMap;
            {error,_} -> undefined
        end,
    case catch jsx:decode(NewCfgRaw,[return_maps]) of
        NewCfg when NewCfg==CurrCfg -> ok;
        _ ->
            CfgPath = ?LCfgU:srvpath(cfgsd),
            ?U:create_json_file(CfgPath,?CfgFN,NewCfgRaw)
    end.