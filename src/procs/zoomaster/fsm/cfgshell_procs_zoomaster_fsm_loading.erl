%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk  <llceceron@gmail.com>
%%% @date 04.09.2021 18:15
%%% @doc

-module(cfgshell_procs_zoomaster_fsm_loading).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([do_load/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "ZooMaster FSM. Loading").

%% ====================================================================
%% Public functions
%% ====================================================================

do_load(State) ->
    ?LOG('$trace',"~p. do_load. on spawn (~120p)",[?MLogPrefix,self()]),
    #zoomsrvstate{ref=Ref}=State,
    case do_load_ensure_zoo_cfg_path(State) of
        {ok,empty_cfg}=Ok ->
            ?MZSrv:cast({?MZE, {load_result,Ok}, Ref}),
            Request = {from_zoomaster,get_current_cfg},
            ?MSrv:ext_cast(Request);
        {ok,exists}=Ok -> % @info cfg may be not_found
            ?MZSrv:cast({?MZE, {load_result,Ok}, Ref}),
            ?MZSrv:cast({?MZE, zoo_cfg_changed, Ref});
        {error,Reason} ->
            exec_on_error_load(Reason, State)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------
%% do_load
%% -----------

%% ---
do_load_ensure_zoo_cfg_path(State) ->
    ?LOG('$trace',"~p. do_load_ensure_zoo_cfg_path.",[?MLogPrefix]),
    #zoomsrvstate{conn_pid=ConnPid}=State,
    case ?AZK:create_r(ConnPid,?CfgZooNodePath) of
        {ok,_} -> do_load_get_cfg_from_zoo(State);
        {error,Reason,_} ->
            {error,{ensure_cfg_node_error,Reason}}
    end.

%% ---
do_load_get_cfg_from_zoo(State) ->
    ?LOG('$trace',"~p. do_load_get_cfg_from_zoo.",[?MLogPrefix]),
    #zoomsrvstate{conn_pid=ConnPid,srv_pid=SrvPid}=State,
    case ?AZK:get_data_subscr(ConnPid,?CfgZooNodePath,SrvPid) of
        {ok,<<>>} -> {ok,empty_cfg};
        {ok,CfgData} ->
            case catch jsx:decode(CfgData,[return_maps]) of
                XCfgRaw when is_map(XCfgRaw) ->
                    case checking_cfg_from_zoo(XCfgRaw) of
                        ok -> {ok,exists};
                        {error,_} -> {ok,empty_cfg} % @XXX: invalid data in zoo, need to replace by cfg from disk
                    end;
                _ -> {ok,empty_cfg} % @XXX: invalid data in zoo, need to replace by cfg from disk
            end;
        {error,Reason0} ->
            {error,{get_cfg_error,Reason0}}
    end.

%% @private
checking_cfg_from_zoo(XCfgRaw) ->
    CurrCfg = case catch ?CfgL:get_curr_cfg() of
                  {'EXIT',_} -> #{};
                  Val -> Val
              end,
    case ?SCfgU:read_syscfg() of
        {ok,SysCfg} ->
            checking_cfg_from_zoo_validate(SysCfg,CurrCfg,XCfgRaw);
        {error,ReadErr} ->
            ?LOG('$trace',"~p. do_load_get_cfg_from_zoo_check_cfg. read syscfg error: ~120p",[?MLogPrefix,ReadErr]),
            checking_cfg_from_zoo_validate(undefined,CurrCfg,XCfgRaw)
    end.

%% @private
checking_cfg_from_zoo_validate(undefined,CurrCfg,XCfgRaw) ->
    case ?CfgL:validate_cfg(XCfgRaw,CurrCfg) of
        {ok,_} -> ok;
        {error,ValidateErr}=Err ->
            ?LOG('$trace',"~p. checking_cfg_from_zoo_validate (1). validate_cfg_raw error: ~120p",[?MLogPrefix,ValidateErr]),
            Err
    end;
checking_cfg_from_zoo_validate(SysCfg,CurrCfg,XCfgRaw) ->
    case ?CfgL:validate_cfg_raw(XCfgRaw,CurrCfg,SysCfg) of
        {ok,_} -> ok;
        {error,ValidateErr}=Err ->
            ?LOG('$trace',"~p. checking_cfg_from_zoo_validate (2). validate_cfg_raw error: ~120p",[?MLogPrefix,ValidateErr]),
            Err
    end.

%% ---
exec_on_error_load(ErrReason, State) ->
    ?LOG('$trace',"~p. on_error_load.",[?MLogPrefix]),
    #zoomsrvstate{ref=Ref}=State,
    ?MZSrv:cast({?MZE,{load_result,{error,ErrReason}},Ref}).