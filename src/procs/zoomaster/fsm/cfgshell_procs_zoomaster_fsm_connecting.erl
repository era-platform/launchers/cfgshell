%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 04.10.2021 14:57
%%% @doc

-module(cfgshell_procs_zoomaster_fsm_connecting).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([start_connector/1,
         get_zoo_connstr/0,
         get_zoo_connector_pid/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "ZooMaster FSM. Connecting").
-define(ZooConnectorName, 'cfgshell_zoomaster_connect').

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec start_connector(State::#zoomsrvstate{}) -> ok.
%% ---
start_connector(State) ->
    do_start_connector(State).

%% ---
-spec get_zoo_connstr() -> {ok, not_found} | {ok, [{Host::list(),Port::integer()}]}.
%% ---
get_zoo_connstr() ->
    ?LOG('$trace',"~p. get_zoo_connstr.",[?MLogPrefix]),
    DefaultConnStr = <<"undefined">>,
    ZooConnStr = ?LCfgU:get_localcfg_opts(connection_string,DefaultConnStr),
    case ZooConnStr of
        DefaultConnStr -> {ok, not_found};
        ZooConnStr ->
            StrsRaw = binary:split(ZooConnStr,<<",">>,[global,trim_all]),
            FMap = fun(StrItem) ->
                           [Host,Port] = binary:split(StrItem,<<":">>,[global,trim_all]),
                           {?BU:to_unicode_list(Host), ?BU:to_integer(Port)}
                   end,
            Strs = lists:map(FMap,StrsRaw),
            {ok,Strs}
    end.

%% ---
-spec get_zoo_connector_pid() -> undefined | pid().
%% ---
get_zoo_connector_pid() ->
    erlang:whereis(?ZooConnectorName).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------
%% start_connector
%% -----------

%% ---
do_start_connector(State) ->
    ?LOG('$trace',"~p. do_start_connector.",[?MLogPrefix]),
    #zoomsrvstate{ref=Ref}=State,
    case catch get_zoo_connstr() of
        {ok, not_found} ->
            ?MZSrv:cast({?MZE, {start_connector_result,{ok,zoo_not_exists}}, Ref});
        {ok, ConnStr} when is_list(ConnStr) ->
            % @XXX: строка подключения измениться не может, т.к. local.cfg перечитывается только при перезапуске системы
            case start_zoo_connector(ConnStr) of
                {ok, ConnPid} ->
                    ?MZSrv:cast({?MZE, {start_connector_result,{ok,ConnPid,ConnStr}}, Ref});
                {error, StartErr} ->
                    exec_on_error(StartErr, State)
            end;
        {'EXIT', CrashInfo} ->
            exec_on_error(CrashInfo, State);
        {error, Reason} ->
            exec_on_error(Reason, State)
    end.

%% ---
start_zoo_connector(ConnStr) ->
    ?LOG('$trace',"~p. start_zoo_connector.",[?MLogPrefix]),
    Options = [{chroot, "/"},
               {monitor, ?MZSrv}],
    case catch ?AZK:connect({local, ?ZooConnectorName}, ConnStr, 30000, Options) of
        {'EXIT',CrashInfo} -> {error,CrashInfo};
        {ok,_}=Ok -> Ok;
        {error,_}=Err -> Err
    end.

%% ---
exec_on_error(ErrReason, State) ->
    ?LOG('$trace',"~p. exec_on_error.",[?MLogPrefix]),
    #zoomsrvstate{ref=Ref}=State,
    ?MZSrv:cast({?MZE,{start_connector_result,{error,ErrReason}},Ref}).