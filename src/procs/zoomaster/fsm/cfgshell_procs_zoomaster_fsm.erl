%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk  <llceceron@gmail.com>
%%% @date 30.08.2021 14:51
%%% @doc

-module(cfgshell_procs_zoomaster_fsm).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([handle_event/2, handle_event/3,
         %
         initing/3,
         loading/3,
         working/3,
         connecting/3,
         inactive/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfgshell.hrl").

-define(MLogPrefix, "ZooMaster FSM").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec handle_event(Event::term(), State::#zoomsrvstate{}) -> State1::#zoomsrvstate{}.
%% ---
handle_event(Event, State) ->
    handle_event(Event, #{}, State).

%% ---
-spec handle_event(Event::term(), MapOpts::map(), State::#zoomsrvstate{}) -> State1::#zoomsrvstate{}.
%% ---
handle_event(Event, MapOpts, #zoomsrvstate{state=St}=State) ->
    ?LOG('$trace',"~p. <-- Event (~120p) state before: ~120p",[?MLogPrefix,Event,St]),
    #zoomsrvstate{state=St1}=State1 = ?MODULE:St(Event, MapOpts, State),
    ?LOG('$trace',"~p. --> Event (~120p) state after: ~120p",[?MLogPrefix,Event,St1]),
    State1.

%% ====================================================================
%% FSM
%% ====================================================================

%% ------------------
%% initing
%% ------------------

initing(init, _, State) ->
    State1 = do_start_connector(State),
    State1#zoomsrvstate{state=?MZStC}.

%% -----------
%% connecting
%% -----------

%% start connect srv success
connecting({start_connector_result,{ok,Pid,ConnStr}}, _, State) ->
    ?LOG('$trace',"~p. start_connector_result. ok. pid (~120p), connstr (~120p)",[?MLogPrefix,Pid,ConnStr]),
    State1 = clear_error(State),
    State1#zoomsrvstate{conn_pid=Pid, conn_str=ConnStr, connect_worker=undefined};

%% connection string is missed
connecting({start_connector_result,{ok,zoo_not_exists}}, _, State) ->
    ?LOG('$trace',"~p. start_connector_result. ok. zoo not exists",[?MLogPrefix]),
    State1 = clear_error(State),
    State1#zoomsrvstate{state=?MZStIA, connect_worker=undefined};

%% success connect from zoo srv to zookeeper
connecting(zoo_connected, _, #zoomsrvstate{conn_pid=undefined}=State) ->
    ?LOG('$trace',"~p. zoo_connected. conn_pid is undefined",[?MLogPrefix]),
    {ok,ConnStr} = ?MZFsmC:get_zoo_connstr(),
    ConnPid = ?MZFsmC:get_zoo_connector_pid(),
    State1 = State#zoomsrvstate{conn_pid=ConnPid, conn_str=ConnStr},
    State2 = do_load(State1),
    State2#zoomsrvstate{state=?MZStL};
connecting(zoo_connected, _, State) ->
    ?LOG('$trace',"~p. zoo_connected",[?MLogPrefix]),
    State1 = do_load(State),
    State1#zoomsrvstate{state=?MZStL};

%% connecting worker down
connecting({?MZE,{proc_down,{'DOWN',Ref,process,Pid,Error}}}, _, #zoomsrvstate{connect_worker={Pid,Ref}}=State) ->
    ?LOG('$trace',"~p. proc_down. connect_worker. Pid:~120p; Error:~120p",[?MLogPrefix,Pid,Error]),
    State1 = save_error(Error,State),
    ?MZSrv:send_after({?MZE, init, Ref}, 15000),
    State1#zoomsrvstate{state=?MZStI, connect_worker=undefined};

%%
connecting({from_master,save_cfg,_}, MapOpts, State) ->
    ?LOG('$trace',"~p. from_master. save_cfg",[?MLogPrefix]),
    From = maps:get(from,MapOpts),
    catch gen_server:reply(From,{error,<<"zoomaster in connecting">>}),
    State.

%% -----------
%% loading
%% -----------

%% load success
loading({load_result,{ok,ResAtom}}, _, State) ->
    ?LOG('$trace',"~p. load_result. ok (~120p)",[?MLogPrefix,ResAtom]),
    State1 = clear_error(State),
    State1#zoomsrvstate{state=?MZStW, load_worker=undefined};

%% load error
loading({load_result,{error,Reason}}, _, State) ->
    ?LOG('$trace',"~p. load_result. error. reason: ~120p",[?MLogPrefix,Reason]),
    #zoomsrvstate{ref=Ref}=State,
    State1 = save_error(Reason,State),
    ?MZSrv:send_after({?MZE, init, Ref}, 15000),
    State1#zoomsrvstate{state=?MZStI, load_worker=undefined};

%% load worker down
loading({?MZE,{proc_down,{'DOWN',Ref,process,Pid,Error}}}, _, #zoomsrvstate{load_worker={Pid,Ref}}=State) ->
    ?LOG('$trace',"~p. proc_down. load_worker. Pid:~120p; Error:~120p",[?MLogPrefix,Pid,Error]),
    State1 = save_error(Error,State),
    ?MZSrv:send_after({?MZE, init, Ref}, 15000),
    State1#zoomsrvstate{state=?MZStI, load_worker=undefined};

%%
loading(zoo_disconnected, _, State) ->
    ?LOG('$trace',"~p. zoo_disconnected",[?MLogPrefix]),
    #zoomsrvstate{ref=Ref}=State,
    State1 = save_error(<<"rcv disconnect from zoo">>, State),
    ?MZSrv:send_after({?MZE, init, Ref}, 15000),
    State1#zoomsrvstate{state=?MZStI, load_worker=undefined, conn_pid=undefined, conn_str=undefined};

%%
loading({from_master,save_cfg,_}, MapOpts, State) ->
    ?LOG('$trace',"~p. from_master. save_cfg",[?MLogPrefix]),
    From = maps:get(from,MapOpts),
    catch gen_server:reply(From,{error,<<"zoomaster in loading">>}),
    State;

%%
loading({start_connector_result,{ok,_Pid,_ConnStr}}, _, State) ->
    ?LOG('$trace',"~p. start_connector_result. rcv success response",[?MLogPrefix]),
    State#zoomsrvstate{connect_worker=undefined};

%% @todo add handle load worker down

%%
loading({?MZE,{proc_down,Req}}, _, State) ->
    ?LOG('$trace',"~p. proc_down. Req: ~120p",[?MLogPrefix,Req]),
    State.

%% -----------
%% working
%% -----------

%% request after load
working({from_master,save_current_cfg,CfgMap}, _, State) ->
    ?LOG('$trace',"~p. from_master. save_current_cfg",[?MLogPrefix]),
    %% @todo add check if cfg still empty in zoo, if not - do nothing
    case ?MZFsmW:save_cfg_to_zoo(CfgMap,State) of
        ok -> ok;
        {error,_}=Err -> Err
    end,
    State;

%% by operation reload_cfg
working({from_master,save_cfg,CfgMap}, MapOpts, State) ->
    ?LOG('$trace',"~p. from_master. save_cfg",[?MLogPrefix]),
    Reply =
        case ?MZFsmW:save_cfg_to_zoo(CfgMap,State) of
            ok -> ok;
            {error,_}=Err -> Err
        end,
    From = maps:get(from,MapOpts),
    catch gen_server:reply(From,Reply),
    State;

%%
working(zoo_cfg_changed, _, State) ->
    ?LOG('$trace',"~p. zoo_cfg_changed",[?MLogPrefix]),
    FErr = fun(ErrReason) ->
                   #zoomsrvstate{ref=Ref}=State,
                   State1 = do_terminate_connector(State),
                   State2 = save_error(ErrReason, State1),
                   ?MZSrv:send_after({?MZE, init, Ref}, 1000),
                   State2#zoomsrvstate{state=?MZStI}
           end,
    case ?MZFsmW:get_cfg_from_zoo(State) of
        {ok, empty} -> FErr(<<"cfg is empty">>);
        {ok,CfgRaw} ->
            case ?MZFsmW:save_cfg_to_local(CfgRaw) of
                ok ->
                    Request = load_cfg,
                    ?MSrv:ext_cast(Request),
                    % @todo add save cfg info, md5 or something for compare cfg
                    State;
                {error,_} -> FErr(<<"save cfg to zoo error">>)
            end;
        {error,_} -> FErr(<<"get cfg from zoo error">>)
    end;

%%
working(zoo_disconnected, _, State) ->
    ?LOG('$trace',"~p. zoo_disconnected",[?MLogPrefix]),
    #zoomsrvstate{ref=Ref}=State,
    State1 = save_error(<<"rcv disconnect from zoo">>, State),
    ?MZSrv:send_after({?MZE, init, Ref}, 15000),
    State1#zoomsrvstate{state=?MZStI, conn_pid=undefined, conn_str=undefined};

%%
working({?MZE,{proc_down,Req}}, _, State) ->
    ?LOG('$trace',"~p. proc_down. Req: ~120p",[?MLogPrefix,Req]),
    State.

%% -----------
%% inactive
%% -----------

%%
inactive({from_master,save_cfg,_}, MapOpts, State) ->
    ?LOG('$trace',"~p. from_master. save_cfg",[?MLogPrefix]),
    From = maps:get(from,MapOpts),
    catch gen_server:reply(From,ok),
    State;

%%
inactive(Event, _, State) ->
    ?LOG('$trace',"~p. UNKNOWN EVENT. In state 'inactive'. Event: ~120p",[?MLogPrefix,Event]),
    State.

%% ====================================================================
%% Common FSM functions
%% ====================================================================

%% ---
do_load(State) ->
    FLoad = fun() -> ?MZFsmL:do_load(State) end,
    LoadWorker = erlang:spawn_monitor(FLoad),
    State#zoomsrvstate{load_worker=LoadWorker}.

%% ---
do_start_connector(State) ->
    FConnect = fun() -> ?MZFsmC:start_connector(State) end,
    ConnectWorker = erlang:spawn_monitor(FConnect),
    State#zoomsrvstate{connect_worker=ConnectWorker}.

%% ---
do_terminate_connector(State) ->
    #zoomsrvstate{conn_pid=ConnPid}=State,
    ?AZK:close(ConnPid),
    State#zoomsrvstate{conn_pid=undefined,conn_str=undefined}.


%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
save_error(ErrReason, State) ->
    #zoomsrvstate{map=M}=State,
    M1 = M#{error => ErrReason,
            error_ts => ?BU:timestamp()},
    State#zoomsrvstate{map=M1}.

%% ---
clear_error(State) ->
    #zoomsrvstate{map=M}=State,
    M1 = maps:without([error,error_ts],M),
    State#zoomsrvstate{map=M1}.