%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 02.01.2021
%%% @doc

-module(cfgshell).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-behaviour(application).

-export([start/0,stop/0,
         start/2, stop/1,
         add_deps_paths/0,
         %
         node_request/2,
         installer_request/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("cfgshell.hrl").

%% ====================================================================
%% Public API
%% ====================================================================

%% ---
start() ->
    ?OUTL("Application start.", []),
    add_deps_paths(),
    load_localcfg(),
    setup_deps_env(),
    Result = application:ensure_all_started(?APP, permanent),
    ?OUTL("Application start. Result:~120p",[Result]),
    case Result of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% ---
stop() ->
    application:stop(?APP).

%% ---
-spec node_request(EventName::atom(),NodeName::atom()) -> ok.
%% ---
node_request(EventName,NodeName) ->
    Req = {?NE, EventName, NodeName},
    ?MSrv:cast(Req).

%% ---
-spec installer_request(Command::atom(),MapOpts::map()) -> {ok,Result::map()} | {error,Reason::binary()}.
%% ---
installer_request(Command,MapOpts) ->
    Timeout = maps:get(timeout,MapOpts,5000),
    ?MSrv:call({installer_request,Command,MapOpts},Timeout).


%% ===================================================================
%% Private
%% ===================================================================

%% ---
start(_Mode, State) ->
    ?OUTL("Application start(Mode, State)", []),
    ?SUPV:start_link(State).

%% ---
stop(_State) ->
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% ---
%% adds app's dependencies paths to code
%% ---
add_deps_paths() ->
    Path = ?U:drop_last_subdir(code:which(?MODULE), 3),
    DepsPaths = filelib:wildcard(Path++"/*/ebin"),
    AddPathsRes = code:add_pathsa(DepsPaths),
    AddPathsRes.

%% ---
setup_deps_env() ->
    Node = erlang:node(),
    LogLevel = ?BU:to_atom_new(?LCfgU:get_localcfg_opts(loglevel,'$info')),
    ?BU:set_env(?APPCFGL,server_node,Node),
    ?BU:set_env(?APPBL,max_loglevel,LogLevel).

%% ---
load_localcfg() ->
    LocalCfg =
        case ?LCfgU:read_localcfg() of
            {ok,LCfg} -> LCfg;
            {error,Reason}->
                ?OUTL("Application start. read_localcfg. read localcfg error:~120p", [Reason]),
                #{}
        end,
    ?LCfgU:save_localcfg_to_env(LocalCfg).