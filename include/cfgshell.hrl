%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 02.01.2021
%%% @doc

-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

%% ====================================================================
%% Define modules and variables
%% ====================================================================

%% -----------
%% General
%% -----------

%% Application
-define(APP, cfgshell).
-define(ServiceName, "CfgSrv"). % prefix for output in logs
-define(SUPV, cfgshell_supv).

%% other apps
-define(APPCfgStarter, cfgstarter).
-define(APPBL, basiclib).
-define(APPCFGL, cfglib).

%% Common
-define(U, cfgshell_utils).
-define(LAvgU, cfgshell_utils_loadavg).
-define(LCfgU, cfgshell_utils_localcfg).
-define(CfgU, cfgshell_utils_cfg).
-define(SCfgU, cfgshell_utils_syscfg).

%% vars
-define(CfgFN, <<"cfg">>).
-define(CfgFNWExt, <<"cfg.json">>).

%% -----------
%% Processes
%% -----------

%% ---
%% Master Cfg server
%% ---

%% Master srv
-define(MSrv, cfgshell_master_srv).
-define(MU, cfgshell_master_utils).

%% Master fsm
-define(MFsm, cfgshell_master_fsm).
-define(MLoading, cfgshell_master_fsm_loading).
-define(MWorking, cfgshell_master_fsm_working).
-define(MSuspending, cfgshell_master_fsm_suspending).
-define(MFsmU, cfgshell_master_fsm_utils).

-define(ME, master_event).

-define(MStI, initing).
-define(MStL, loading).
-define(MStW, working).
-define(MStS, suspending).

%% Nodes Fsm
-define(MNFsm, cfgshell_master_nodes_fsm).

-define(NE, node_event).
-define(NStI, initing).
-define(NStW, working).
-define(NStS, stopped).

%% Ets
-define(MNodesEts, 'cfg_master_srv_nodes').

%% ---
%% Zookeeper Master server
%% ---

-define(MZSrv, cfgshell_procs_zoomaster_srv).

%% Fsm
-define(MZFsm, cfgshell_procs_zoomaster_fsm).
-define(MZFsmC, cfgshell_procs_zoomaster_fsm_connecting).
-define(MZFsmL, cfgshell_procs_zoomaster_fsm_loading).
-define(MZFsmW, cfgshell_procs_zoomaster_fsm_working).

-define(MZE, zoomaster_event).

-define(MZStI, initing).
-define(MZStL, loading).
-define(MZStC, connecting).
-define(MZStW, working).
-define(MZStIA, inactive).

-define(CfgZooNodePath, <<"/configs/cfg">>).

%% -----------
%% Interfaces
%% -----------

%% Installer
-define(InstallerLCfg, cfgshell_installer_localcfg).
-define(InstallerU, cfgshell_installer_utils).
%% command install
-define(CmdInstall, cfgshell_installer_commands_install).
-define(CmdInstDefM, cfgshell_installer_commands_install_default).
-define(CmdInstDevM, cfgshell_installer_commands_install_dev).
-define(CmdInstScr, cfgshell_installer_commands_install_scripts).
%% command run
-define(CmdStart, cfgshell_installer_commands_start).

%% -----------
%% Adapters
%% -----------

%% zookeeper
-define(AZK, cfgshell_adapters_zookeeper).

%% -----------
%% Dependencies
%% -----------

%% basiclib app
-define(BU, basiclib_utils).
-define(BWSupv, basiclib_worker_supv).
-define(BWSrv, basiclib_worker_srv).
-define(BRpc, basiclib_rpc).
-define(BLlog, basiclib_log).
-define(BLStore, basiclib_store).

%% Cfg lib
-define(CfgL, cfglib).
-define(CfgLF, cfglib_f).

%% @todo mv to platform module
-define(LocalCfgFileName, <<"local.cfg">>).
-define(LocalCfgFileNameWExt, <<?LocalCfgFileName/binary,".json">>).


%% ====================================================================
%% Define Records
%% ====================================================================

%% -----------
%% ZooMaster srv records
%% -----------

-record(zoomsrvstate, {state=?MZStI :: ?MZStI | ?MZStL | ?MZStC | ?MZStW | ?MZStIA,
                       srv_pid :: pid(),
                       conn_str :: undefined | [{Host::list(), Port::integer()}],
                       conn_pid :: undefined | pid(),
                       load_worker :: undefined | {pid(),reference()},
                       connect_worker :: undefined | {pid(),reference()},
                       map=#{} :: map(),
                       ref :: reference()}).

%% -----------
%% Master srv records
%% -----------

-record(msworkers, {cfg_load :: undefined | {pid(),reference()},
                    node_refresh :: undefined | {pid(),reference()},
                    check_ping :: undefined | {pid(),reference()},
                    analyze_svcnode_ping :: undefined | {pid(),reference()}
                   }).

-record(msrvstate, {state=?MStI :: ?MStI | ?MStL | ?MStW | ?MStS,
                    cfg_data=#{} :: map(),
                    nodes_ets :: atom(),
                    workers=#msworkers{} :: #msworkers{},
                    map=#{} :: map(),
                    ref :: reference()
                   }).

%% -----------
%% Node FSM records
%% -----------

-record(ping_state, {state=ok :: ok | error,
                     error_cnt=0 :: integer(),
                     last_ts :: undefined | integer(),
                     ping_ref :: undefined | reference()
                    }).

-record(node, {name :: binary(),
               state=?NStI :: ?NStI | ?NStW | ?NStS,
               host :: binary(),
               node :: atom(),
               start_cmd :: binary(),
               msvc_data=#{} :: map(),
               ping_state=#ping_state{} :: #ping_state{}
              }).

%% ====================================================================
%% Define logs
%% ====================================================================

%% basiclib log
-define(LOGFILE, {cfgshell,erl}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level,?LOGFILE,{Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level,?LOGFILE,Text)).

-define(OUT(Text), ?BLlog:out(Text)).
-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level,?LOGFILE,{Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level,?LOGFILE,Text)).

-define(OUTC(Level,Fmt,Args), ?BLlog:out(Fmt,Args)).
-define(OUTC(Level,Text), ?BLlog:out(Text)).

%% local
-define(OUTL(Fmt,Args), io:format("~s. "++Fmt++"~n",[?ServiceName]++Args)).
-define(OUTL(Text), io:format("~s. "++Text++"~n",[?ServiceName])).