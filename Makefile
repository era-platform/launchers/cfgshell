all: compile

compile: clean_rebar_lock
	rebar3 compile
clean_rebar_lock:
	rm -f rebar.lock

clean:
	rebar3 clean
distclean:
	rm -f TEST-*.xml
	rm -rf _build rebar.lock

list-deps:
	rebar3 deps
eunit:
	rebar3 eunit
xref:
	rebar3 xref
dialyzer:
	rebar3 dialyzer

cc: clean compile

ccx: clean compile xref eunit

devinstall:
	mkdir -p /usr/lib/era
	#ln -sf priv/installer.escript installer.escript
	ln -sf /home/ceceron/develop/era3/launchers/cfgshell/_build/default/lib /usr/lib/era/cfgshell
	ln -sf /home/ceceron/develop/era3/launchers/cfgstarter/_build/default/lib /usr/lib/era/cfgstarter
	ln -sf /cc_slave/erlangs/24.3.4 /usr/lib/era/erlang
	cd /usr/lib/era && /cc_slave/erlangs/24.3.4/bin/escript cfgshell/cfgshell/priv/installer.escript install srvname=newtestserv srvhost=cclocal.ru psk=8800
