#!/usr/bin/env escript
%% -*- erlang -*-
%%! -hidden +Bc +pc unicode -kernel inet_dist_listen_min 9300 inet_dist_listen_max 9309

-module(installer).
-mode(compile).

%% ====================================================================
%% Defines
%% ====================================================================

-define(BU, basiclib_utils).

-define(InstallerCmdMPrefix, "cfgshell_installer_commands_").
-define(InstallerHelpM, erlang:list_to_atom(?InstallerCmdMPrefix++"help")).
-define(InstallerF, do_command).

%% ---
%% LOGS
%% ---
-define(OUTL(Fmt,Args), erlang:apply(fun() ->
                                             case catch io:format("~s"++Fmt++"~n",[log_date()]++Args) of
                                                 {'EXIT',_ERROUTL} -> io:format("~s LOG ERROR ~p~n",[log_date(),_ERROUTL]);
                                                 ok -> ok
                                             end end,[])).
-define(OUTL(Text), erlang:apply(fun() -> case catch io:format("~s"++Text++"~n",[log_date()]) of
                                              {'EXIT',_ERROUTL} -> io:format("~s LOG ERROR ~p~n",[log_date(),_ERROUTL]);
                                              ok -> ok
                                          end end,[])).

%% ====================================================================
%% API functions
%% ====================================================================

main([Command|Opts]) ->
    ?OUTL("Installer. Start with Command:~120p",[Command]),
    ?OUTL("Installer. Start Opts:~120p",[Opts]),
    Result =
        case add_deps() of
            ok ->
                ?OUTL("Installer. Add deps success."),
                InstallerModule = ?BU:to_atom_new(?InstallerCmdMPrefix++Command),
                MapOpts = parse_opts(Opts),
                case function_exported(InstallerModule,?InstallerF,1) of
                    true -> erlang:apply(InstallerModule,?InstallerF,[MapOpts]);
                    false ->
                        ?OUTL("Invalid command: ~p. Read help:~n",[Command]),
                        erlang:apply(?InstallerHelpM,?InstallerF,[MapOpts])
                end;
            {error,_}=Err -> Err
        end,
    handle_result(Result).

%% ====================================================================
%% Internal Functions
%% ====================================================================

%% ---
add_deps() ->
    CodeDirs = installer_paths(),
    io:format("CodeDir:~120p~n",[CodeDirs]),
    add_deps(CodeDirs).

add_deps({DefCodeDir,DevCodeDir}) ->
    case add_deps_default(DefCodeDir) of
        ok -> ok;
        {error,_} -> add_deps_dev(DevCodeDir)
    end.

%% @private
add_deps_default(CodeDir) ->
    BasicLibEbinPath = filename:join([CodeDir,"cfgshell","basiclib","ebin"]),
    CfgShellEbinPath = filename:join([CodeDir,"cfgshell","cfgshell","ebin"]),
    JsxEbinPath = filename:join([CodeDir,"cfgshell","jsx","ebin"]),
    case lists:all(fun(Path) -> code:add_patha(to_list(Path))==true end, [BasicLibEbinPath,CfgShellEbinPath,JsxEbinPath]) of
        true -> ok;
        _ -> {error,<<"Add deps(1) error">>}
    end.

%% @private
add_deps_dev(CodeDir) ->
    BasicLibEbinPath = filename:join([CodeDir,"cfgshell","_build","default","lib","basiclib","ebin"]),
    CfgShellEbinPath = filename:join([CodeDir,"cfgshell","_build","default","lib","cfgshell","ebin"]),
    JsxShellEbinPath = filename:join([CodeDir,"cfgshell","_build","default","lib","jsx","ebin"]),
    io:format("BasicLibEbinPath:~120p~n",[BasicLibEbinPath]),
    io:format("CfgShellEbinPath:~120p~n",[CfgShellEbinPath]),
    case lists:all(fun(Path) -> code:add_patha(to_list(Path))==true end, [BasicLibEbinPath,CfgShellEbinPath,JsxShellEbinPath]) of
        true -> ok;
        _ -> {error,<<"Add deps(2) error">>}
    end.

%% ---
parse_opts(Opts) ->
    FParse = fun(Param,Acc) ->
                     case string:tokens(Param,"=") of
                         [K,V] -> Acc#{?BU:to_atom_new(K) => ?BU:to_binary(V)};
                         _ -> throw({error,"bad param: "++Param})
                     end
             end,
    lists:foldl(FParse,#{},Opts).

%% ---
handle_result(ok) -> ok;
handle_result({ok,Msg}) ->
    ?OUTL("Operation Success!~n~120tp~n",[Msg]),
    ok;
handle_result({error,ErrMsg}) ->
    ?OUTL("Operation Failed! Error in: ~120tp~n",[ErrMsg]),
    erlang:halt(1).

%% -------------------------------------
%% Return paths by key
%% -------------------------------------
installer_paths() ->
    AbsPath = to_binary(filename:absname(escript:script_name())),
    io:format("AbsPath:~120p~n",[AbsPath]),
    %Cmd = <<"readlink -f ",SymLinkPath/binary>>,
    %{0,PrepAbsPath} = cmd_exec(Cmd),
    %AbsPath = binary:replace(PrepAbsPath,<<"\n">>,<<>>,[global]),
    Dev0 = lists:droplast(lists:droplast(lists:droplast(filename:split(AbsPath)))),
    Dev = filename:join(Dev0),
    Default = filename:join(lists:droplast(Dev0)),
    {Default,Dev}.

%% ---
to_binary(Item) when is_binary(Item) ->    Item;
to_binary(Item) when is_list(Item) -> case catch list_to_binary(Item) of {'EXIT',_} -> unicode:characters_to_binary(Item); T -> T end;
to_binary(Item) when is_atom(Item) -> list_to_binary(atom_to_list(Item));
to_binary(Item) when is_integer(Item) -> list_to_binary(integer_to_list(Item));
to_binary(Item) when is_float(Item) -> list_to_binary(float_to_list(Item,[{decimals,6},compact])).

%% ---
to_list(Item) when is_tuple(Item) -> erlang:tuple_to_list(Item);
to_list(Item) when is_list(Item) ->    Item;
to_list(Item) when is_binary(Item) -> binary_to_list(Item); % some modules use cyrillic names, so simple transformation from list to binary is wrong
to_list(Item) when is_atom(Item) -> atom_to_list(Item);
to_list(Item) when is_integer(Item) -> integer_to_list(Item);
to_list(Item) when is_float(Item) -> float_to_list(Item,[{decimals,6},compact]);
to_list(Item) when is_pid(Item) -> pid_to_list(Item).

%% ---
%% checks if function exported
%% ---
function_exported(M,F,Arity) ->
    case erlang:function_exported(M, F, Arity) of
        true -> true;
        false ->
            case code:is_loaded(M) of
                false -> code:ensure_loaded(M);
                _ -> ok
            end,
            erlang:function_exported(M, F, Arity)
    end.

%% ---
%% Log functions
%% ---
log_date() ->
    {_, _, Micro}=TimeStamp = os:timestamp(),
    {_Date, {Hours, Minutes, Seconds}} = calendar:now_to_local_time(TimeStamp),
    {H, M, S, Ms} = {Hours, Minutes, Seconds, Micro div 1000 rem 1000},
    lists:flatten(io_lib:format("~2..0B:~2..0B:~2..0B.~3..0B    ",[H,M,S,Ms])).